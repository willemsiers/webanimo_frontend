#!/bin/bash
cwd=$(pwd)
cd $1
mvn package -DskipTests
cd $cwd
cp $1/target/animo-3.1.1.jar animo-latest.jar
