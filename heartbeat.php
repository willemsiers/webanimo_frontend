<?php
$location = './networks';
$locksloc = './locks';

// got a network?
if (empty($_GET['network'])) {

	die ('{"error": "Invalid heartbeat"}');

}

// get data
$ip = $_SERVER['REMOTE_ADDR']; 
$file = preg_replace('/[^a-zA-Z0-9]/', '', $_GET['network']);

// check other relevant shizzle, described by error message
if (!file_exists($location . '/' . $file)) {
	die('{"error" : "This network does not exist!" }');
}
if (!file_exists($locksloc . '/' . $file)) {
	die('{"error" : "No lock acquired"}');
}
if (file_get_contents($locksloc . '/' . $file) != $ip) {
	die('{"error" : "Lock acquired by different user" }');
}

// update heartbeat
touch($locksloc . '/' . $file);
echo '{"done" : "ok"}';
return;