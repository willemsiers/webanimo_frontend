<?php // when changing: also check savenetwork.php, heartbeatshizzle.php & processing.php
	error_reporting(~E_NOTICE & ~E_DEPRECATED & ~E_USER_DEPRECATED & ~E_STRICT);
	//error_reporting(E_ALL); //turn on while developing
	$location = './networks';
	$locksloc = './locks';
	$animojar = './animo-latest.jar';
	$file 	  = preg_replace('/[^a-zA-Z0-9]/', '', $_GET['network']);
?><html ng-app="graphCtrlApp">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>webANIMO</title>
	<!-- includes -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
	<script type="text/javascript" src="js/jquery.jeegoocontext-2.0.0.js"></script>
	<script type="text/javascript" src="js/jquery-hotkeys.js"></script>
	<script src="js/cytoscape.js"></script>
    <script src="js/jszip.min.js"></script>
    <script src="js/papaparse.min.js"></script>
	<script src="js/console.js"></script>
	<script src="js/init.js"></script>

    <script src="js/FileSaver.js"></script>
    <script src="js/papaparse.min.js"></script>
    <script src="js/exporting.js"></script>
    <script src="js/importing.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/line-chart/2.0.18/LineChart.js"></script>
	<script src="./node_modules/d3/d3.min.js"></script>
	<script src="js/tooltip.js"></script>
	<script src="js/controllers.js"></script>
	<script src="js/jquery.qtip.min.js"></script>
	
	<script src="js/jquery-ui.min.js"></script>
	
	<script src="js/examples.js"></script>
	<script src="js/hotkeys.js"></script>
	<script src="js/manipulate_cy.js"></script>
	<script src="js/manipulate_gui.js"></script>
	<script src="js/clickmenu.js"></script>
	<script src="js/selection.js"></script>
	<script src="js/functionality.js"></script>
	<script src="js/CSEditor.js"></script>
	<script src="js/changelogs.js"></script>
	<script src="js/json.js"></script>
	<link href="styles/normalize.css" rel="stylesheet" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/line-chart/2.0.18/LineChart.css">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
	<link href="styles/jeegoocontextstyle.css" rel="stylesheet"/>
	<link href="styles/jquery.qtip.min.css" rel="stylesheet" />
	<link href="styles/jquery-ui.min.css" rel="stylesheet" />
	<link href="styles/jquery-ui.structure.min.css" rel="stylesheet" />
    <link rel="icon" type="image/x-icon" href="images/favicon.ico" />
	
	<link href="styles/mainstyle.css" rel="stylesheet" />
	<script>
	<?php
		// load network
		if (!empty($_GET['network'])) {
			$sTimeout = "30 seconds";
			// valid network
			if (file_exists($location . '/' . $file)) {

				echo 'var loadedNetworkId = ';
				echo '"';
				echo $file;
				echo '"';
				echo ";\r\n";

				echo 'var loadedNetwork = ';
				echo file_get_contents($location . '/' . $file);
				echo ";\r\n";

				// canILockIt?
				//echo filemtime($locksloc . '/' . $file);
				//echo strtotime("now - $sTimeout");
				if (!file_exists($locksloc . '/' . $file) || filemtime($locksloc . '/' . $file) < strtotime("now - $sTimeout") || file_get_contents($locksloc . '/' . $file) == $_SERVER['REMOTE_ADDR']) {

					echo "var lockError     = false;\r\n";

					// acquire lock
					file_put_contents($locksloc . '/' . $file, $_SERVER['REMOTE_ADDR']);
					chmod($locksloc . '/' . $file, 0600);

				} else {
					echo "var lockError     = true;\r\n";
				}


				
			} else {
				echo "var loadedNetwork = null;\r\n";
				echo "var lockError     = false;\r\n";
			}
		}
		 else {
			echo "var loadedNetwork = null;\r\n";
			echo "var lockError     = false;\r\n";
		}


	?>
	</script>

</head>
<body>
<?php
	if (!is_writable($location) || !is_readable($location))
		die('Please give read and write permissions for '.$location.'</body></html>');

	if (!is_writable($locksloc) || !is_readable($locksloc))
		die('Please give read and write permissions for '.$locksloc.'</body></html>');


	if (!file_exists($animojar) || !is_readable($animojar)) 
		die('Please install '.$animojar.' or make it readable. </body></html>');


?>

<!-- wrapper for global popups -->
<div id="wrapper" ng-controller="GraphCtrl as vm">
	<!-- upper bar -->
	<div id="upperBar">
		<div class="left" id="logo">
			<h1><a href=".">webANIMO</a></h1><input type="text" style="display: none" name="network" id="export" ng-model="vm.network">
		</div>
		<div class="left" id="topBar">
			<ul id="mainMenu">
				<li>
					<a href="#">Network</a>
					<ul>
						<li><a href="#" id="newNetworkButton">New</a></li>
						<li><a href="#" id="importNetworkButton">Open..</a></li>
						<li><a href="#" id="exportGraph">Save..</a></li>
						<li><a href="#" id="exportGraphImage">Export as Image</a></li>
						<li><a href="#" id="getShareableLink">Get shareable link</a></li>
					</ul>
				</li><!--
				<li>
					<a href="#">Edit</a>
					<ul>
						
					</ul>		
				</li>
				<li>
					<a href="#">View</a>
					<ul>
						
					</ul>		
				</li>
				<li>
					<a href="#">Select</a>
					<ul>
						
					</ul>		
				</li>-->
				<li>
					<a href="#">Examples</a>
					<ul>
						<li><a href="#" id="example_modelbase">Model base</a></li>
						<li><a href="#" id="example_chrondrocyte">Chrondrocyte</a></li>
						<li><a href="#" id="example_echo">ECHO</a></li>
					</ul>
				</li>
				<li>
					<a href="#">Plot</a>
					<ul >
						<li><a href id="btn-set-initial-activities" ng-click="vm.setSelectedInitialActivitiesOnNetwork();" >Set Initial Activities</a></li>
						<li><a href id="btn-import-csv" ng-click="vm.importCSV()">Import CSV data</a></li>
						<li><a href id="btn-export-as-csv" ng-click="vm.downloadCSV()">Export data as CSV</a></li>
						<li><a href  id="btn-download-image" ng-click="vm.exportImage(true);">Download plot as Image</a></li>
						<li><a href id="btn-download-image" ng-click="vm.exportImage(false);">Open plot as Image</a></li>
					</ul>		
				</li>
				<li>
					<a href="#">Help</a>
					<ul>
						<li><a href="./manual.pdf" target="_blank" id="downloadManuel">Manual</a></li></a>
						<li><a href="#" id="about">About</a></li>
					</ul>
				</li>
				<!--
				<li><a href="#">Preferences</a></li>
				<li>
					<a href="#">Help</a>
					<ul>
						<a href id="asdfsdf" onclick="displayConsoleMessage('test!!!', 0, Math.random()*993000)">
							<li>Test</li>
						</a>
					</ul>		
				</li>-->
			</ul>
			<div class="clear"></div>
		</div>
		<div id="saveToCloudStatusContainer">
			<span class="lockerror" style="display:none;">Error acquiring lock: someone else is already editing this file. Click <a href="./?network=<?php echo $file?>">here</a> to retry.</span><br />
			<span id="saveToCloudStatus" style="display:none;">Saving changes</span>
		</div>
		<div class="clear"></div>
	</div>
	<!-- toolbar -->
	<div id="toolbar">
		<input type="file" id="importNetwork" class="fileButton" value="Import Network" accept=".cyjs,.cys"/>
        <form id="importCsvForm" style="display: none;"><input type="file" id="importCsv" class="fileButton" value="Import CSV Data" accept=".csv"/></form>
		<!--<input type="button" id="analyse" class="toolbarButton" value="Analyse Network"/>-->
		<a href="#" class="addNode" title="Add new reactant (rightclick in editor)"><img src="./images/add.png" /></a>
		<a href="#" class="addEdge" title="Add new interaction (rightclick in editor)"><img src="./images/edge.png"></a>
		<div class="seperator"></div>
		<a href="#" class="zoomIn" title="Zoom in (shortcut '+')"><img src="./images/zoomin.png"/></a>
		<a href="#" class="zoomOut" title="Zoom out (shortcut '-')"><img src="./images/zoomout.png" /></a>
		<a href="#" class="fit" title="Zoom to fit (shortcut 'home')"><img src="./images/fit.png"/></a>
		<div class="seperator"></div>
		<a href="#" class="undo" title="Undo change (shortcut 'ctrl + z')"><img src="./images/undo.png" /></a>
		<a href="#" class="redo"  title="Redo change (shortcut 'ctrl + y')"><img src="./images/redo.png" /></a>
		<div class="seperator"></div>
		<a href="#" class="reorganize"  title="Reorganize network (changes reactant positions)"><img src="./images/shuffle.png"></a>
		<div class="seperator"></div>
		<a href="#" class="slideReset"  title="Reset window sizes"><img src="./images/reset.png"></a>
		<a href="#" class="slideLeft"  title="Set plot view full width"><img src="./images/left.png"></a>
		<a href="#" class="slideRight"  title="Set network view full width"><img src="./images/right.png"></a>
		
		<!--
		<input type="file" id="importNetwork" class="fileButton" value="Import Network" accept=".cyjs"/>
		<label for="importNetwork" class="toolbarButton">Import Network</label>
		<input type="button" id="exportGraph" class="toolbarButton" value="Export Network"/>
		<div id="toolbarConsole" readonly></div>
		-->
		<!-- temp error messages -->

        <!-- dark side of the toolbar -->
		<a href="#" class="toggleRevisionList" title="Show or hide the revision list"><img src="./images/toggle_revisions.png"/></a>
		<a href="#" class="zoomOutPlot" title="Zoom the plot out (shortcut 'shift -' or scroll wheel)"><img src="./images/zoomout.png" /></a>
		<a href="#" class="zoomInPlot" title="Zoom the plot in (shortcut 'shift +' or scroll wheel)"><img src="./images/zoomin.png"/></a>

	</div>
	<!-- left bar -->
	<div id="leftBar" class="fullHeight">
		<div id="leftBarContent">
			<!-- analyse shit -->
			<h2>Analyse Network</h2>
			Minutes to simulate<br />
			<input type="number" id="minutesToSimulate" class="fancyInput" value="40"/>
			<div class="smallMargin"></div>
			<input type="button" value="Analyse" id="doAnalyse" class="fancyButton" />
			<div class="bigMargin"></div>
			<h2>Legend</h2>
			<h3>Activity Ratio</h3>
			<div id="activityWrapper">
				<div id="legend"></div>
			</div>
			<h3>Reactant category</h3>
			<ul>
				<li><img src="./images/cytokine.png"/>Cytokine</li>
				<li><img src="./images/receptor.png"/>Receptor</li>
				<li><img src="./images/kinase.png"/>Kinase</li>
				<li><img src="./images/phosphatase.png"/>Phosphatase</li>
				<li><img src="./images/transcription_factor.png"/>Transcription factor</li>
				<li><img src="./images/gene.png"/>Gene</li>
				<li><img src="./images/mrna.png"/>mRNA</li>
				<li><img src="./images/other.png"/>Other</li>
			</ul>

		</div>
		<div class="fadeMeOutShizzle" style="display: none;"><!-- inline css 'cause of jquery--></div>
		<!-- alle edit sjit enzo -->
		<div id="popContent" class="popupContent">
			<!-- Edit edge -->
			<form id="editEdge" class="popEdit">
				<h1><span id="editEdgeLabel">Edit interaction</span></h1>
				<span id="edgeLabel"></span>
				<input type="hidden" id="edgeId" />
				<div class="inputBox">
					<div class="fullWidth smallMargin inputbox-isEnabled">
						<label class="tableLayout"><input type="radio" name="increment" id="activation" class="edgeListener">Activation</label>
						<label class="tableLayout"><input type="radio" name="increment" id="inhibition" class="edgeListener">Inhibition</label>
					</div>
					<div class="left">
						Source<br />
						<select id="source" class="fancyInput edgeListener"></select>	
					</div>
					<div class="right">
						Target<br />
						<select id="target" class="fancyInput edgeListener"></select>
					</div>
					<div class="clear"></div>
					<!-- clear -->
					<div class="clear smallMargin"></div>
					<div class="smallMargin"></div>
					<div class="fullWidth smallMargin">
						Reaction kinetics<br />
						<select id="reactionType" class="fancyInput largeWidth edgeListener">
							<option value="0">Scenario 1: k * [E]</option>
							<option value="1">Scenario 2: k * [E] * [S]</option>
							<option value="2">Scenario 3: k * [E1] * [E2]</option>
						</select><br />
						<div id="reactionLabel0" class="reactionLabel">
							E = active source<br />
						</div>
						<div id="reactionLabel1" class="reactionLabel">
							E = active source<br />
							S = inactive target
						</div>
						<div id="reactionLabel2" class="reactionLabel">
							E1 =<select id="e1active" class="fancyInput smallInput edgeListener">
								<option value="true">Active</option>
								<option value="false">Inactive</option>
							</select>
							<select id="e1reactant" class="fancyInput smallInput edgeListener"></select><br />
							E2=<select id="e2active" class="fancyInput smallInput edgeListener">
								<option value="true">Active</option>
								<option value="false">Inactive</option>
							</select>
							<select id="e2reactant" class="fancyInput smallInput edgeListener"></select><br />
						</div>
					</div>
					<div class="fullWidth k">
						<input type="range" id="k_slider" min="0" max="4" class="fancyInput largeWidth" />
						<div class="largeWidth sliderDescr"><span class="left">Slow</span> <span class="mid">Medium</span> <span class="right">Fast</span></div>
						<br />K = <input type="text" id="k" class="fancyInput edge" />
					</div>
					
					<!--<select id="reactionSpeed" class="fancyInput smallInput" disabled>
				   <option value="0">Very slow</option>
				   <option value="1">Slow</option>
				   <option value="2">Medium</option>
				   <option value="2">Fast</option>
				   <option value="2">Very fast</option>
				   </select>-->
				   <div class="smallMargin"></div>
				   Description<br />
				   <textarea class="fancyArea edgeListener" id="edgeDescription"></textarea>
				   <div class="smallMargin"></div>
				  <div class="smallMargin"></div>
					<div class="fullWidth smallMargin inputbox-isEnabled">
						<label class="tableLayout"><input type="radio" name="isEnabled" class="edgeListener" id="enabledEdge">Enabled</label>
						<label class="tableLayout"><input type="radio" name="isEnabled" class="edgeListener" id="disabledEdge">Disabled</label>
					</div>
					<input type="button" value="Save" id="addEdgeSave" class="fancyButton"/>
					<input type="button" value="Close" id="editEdgeCancel" class="fancyButton"/>
					<input type="button" value="Remove" id="editEdgeRemove" class="fancyButton"/>
				   <!-- <input type="button" value="Save" id="editEdgeSave" class="fancyButton"/>
				   -->
				</div>
			</form>
			<!-- Edit node popup -->
			<form id="editNode" class="popEdit">
				<h1 id="editNodeLabel">Edit reactant</h1>
				<input type="hidden" id="nodeId" />
				<div class="inputBox">
					<div class="left inputbox-name">
						Name
						<input type="text" id="name" class="fancyInput nodeListener node"/>
					</div>
					<div class="right inputbox-moleculeType">
						Molecule type<br />
						<select id="moleculeType" class="fancyInput nodeListener" value="Pick a molecule">
							<option value="Cytokine">Cytokine</option>
							<option value="Receptor">Receptor</option>
							<option value="Kinase">Kinase</option>
							<option value="Phosphatase">Phosphatase</option>
							<option value="Transcription factor">Transcription factor</option>
							<option value="Gene">Gene</option>
							<option value="mRNA">mRNA</option>
							<option value="Other">Other</option>
						</select>
					</div>
					<!-- clear -->
					<div class="clear smallMargin"></div>
					<div class="inputbox-totalActivity">
					Total activity levels <span id="levelLabel">0</span><br />
					<input type="range" id="totalActivity" class="nodeListener" min="0" max="100" step="1" value="100">
					</div>
					<div class="smallMargin"></div>
					<div class="inputbox-initialActivity">
					Initial activity level <span id="initialLabel">0</span><br />
					<input type="range" id="initialActivity" class="nodeListener" min="0" max="100" step="1" value="50">
					</div>
					<div class="smallMargin"></div>
					<div class="fullWidth smallMargin inputbox-isEnabled">
						<label class="tableLayout"><input type="radio" name="isEnabled" id="enabled">Enabled</label>
						<label class="tableLayout"><input type="radio" name="isEnabled" id="disabled">Disabled</label>
					</div>
					<div class="fullWidth smallMargin inputbox-isPlotted">
						<label class="tableLayout"><input type="radio" name="isPlotted" id="plotted" class="nodeListener" >Plotted</label>
						<label class="tableLayout"><input type="radio" name="isPlotted" id="hidden" class="nodeListener" >Hidden</label>
					</div>
					<div class="inputbox-description">
					Description<br />
					<textarea class="fancyArea nodeListener" id="nodeDescription"></textarea>
					</div>
					<div class="smallMargin"></div>
					<input type="button" value="Save" id="addNodeSave" class="fancyButton"/>
					<input type="button" value="Close" id="editNodeCancel" class="fancyButton"/>
					<!--<input type="button" value="Save" id="editNodeSave" class="fancyButton"/>-->

					<input type="button" value="Remove" id="editNodeRemove" class="fancyButton"/>
				</div>
			</form>
		</div>
		
	</div>
	<!-- rest of the width -->
	<div class="contentWrapper fullHeight">
		<div class="left" id="editorResize">
			<!-- editor -->
			<div id="webANIMOEditor">
				<!-- edge draw arrow div -->
				<div id="arrow"></div>
				<div id="cy"></div>
				<div id="dragger"></div>
				<div id="alpaca"><input type="button" value="Hide alpaca :(" id="hideAlpaca"/><img id="llama"/></div>
				<!-- pos field -->
				<input type="hidden" id="posX" />
				<input type="hidden" id="posY" />
			</div>
			<!--  Contextmenu -->
			<ul id="menu" class="jeegoocontext cm_default">
				<div id="deleteNodeMenu">
					<a href="#" id="deleteNode"><li>Delete reactant</li></a>
					<a href="#" id="addNodeEdge"><li>Add interaction</li></a>
				</div>
				<div id="deleteEdgeMenu">
					<a href="#" id="deleteEdge"><li>Delete interaction</li></a>
				</div>
				<div id="addMenu">
					<a href="#" class="addNode"><li>Add reactant</li></a>
					<a href="#" class="addEdge"><li>Add interaction</li></a>
				</div>
			</ul> 
		</div>
		<div class="right" id="resultResize">
			<!-- result pane container -->
			<div id="changelogPane">
				<div class="revision-legend">
					<div style="width:100%; font-size:1.5em; margin-bottom:5px;">Legend</div>
					<span class="revision-legend-item" style="background-color:green">Added</span>
					<span class="revision-legend-item"style="background-color:red">Deleted</span>
					<span class="revision-legend-item" style="background-color:cyan">Modified</span>
					<span class="revision-legend-item" style="background-color:grey">Unchanged</span>
				</div>
				<ul id="changelogList">
				</ul>
			</div>
			<div id="resultPane" >

				<div class="paneHeader">
					<h1>Results</h1>
				</div>

				<div class="paneContent">
					<div>
						<div id="resultChart">
							<linechart data="data" options="options"></linechart>
						</div>
					</div>
				</div>

				<div class="paneFooter">
					<!--
					<input type="text" ng-model="vm.selectedTime">
					<input type="checkbox" ng-model="vm.xPanEnabled">Pan X
					<input type="checkbox" ng-model="vm.yPanEnabled">Pan Y
					-->
					
					<canvas id="canvas-export-graph" 
				   style="border:2px solid black;display: none;" width="602" height="600" >
					</canvas>
				</div>
			</div>
			<!-- load -->
			<div>
				<p>Input network: <input type="text" ng-model="json"> <input type="submit" id="load" value="LOAD" /></p>
				<p ng-bind="json" id="networkJSON" style="display: none"></p>
			</div>
			<div id="result"></div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
	<!-- footer -->
	<div id="footer">
		<div id="toast-container">
		</div>
	Michiel Bakker, Jacco Brandt, Ruben Haasjes, Bob Rubbens, Willem Siers&nbsp;&nbsp;&copy; 2016 <span style="float: right;margin-right: 40px;">For academic use only</span>
	</div>
</div>
<!-- global popup -->
<div id="popPage" class="popupBG stripHeader"></div>
<div id="popPageContent" class="popupContent">
	<!-- analyse 
	<div id="popAnalyse" class="pagePopup">
		<h1>Analyse Network</h1>
		<div class="smallMargin"></div>
		Amount of minutes to simulate<br />
		<input type="text" id="minutesToSimulate" class="fancyInput" value="240"/>
		<div class="smallMargin"></div>
		<input type="button" value="Analyse" id="doAnalyse" class="fancyButton" />
		<input type="button" value="Cancel" class="cancelPopPage fancyButton" />
	</div> -->
	<!-- import -->
	<div id="popImport" class="pagePopup">
        <h1>Import Network</h1>
        <div class="smallMargin"></div>
        The selected file contains multiple networks. Please click the network to load.
        <form>
        <ul class="imports">
        </ul>
        </form>
        <div style="text-align: center">
            <input type="button" value="Cancel" id="doCancelImport" class="fancyButton" />
            <input type="button" value="Import" id="doImport" class="fancyButton" />
        </div>
	</div>

	<div id="popAbout" class="pagePopup">
        <h1>About webANIMO</h1>
        <div class="smallMargin"></div>
		webANIMO was developed at University of Twente,
		2016,
		Formal Methods and Tools (FMT) research group.
	<br/>
Contact: <img style="display:inline;" src="./images/email.png" /></a>
<br/>
See the <a href="./manual.pdf" target="_blank" id="downloadManuelAbout">manual</a> for more information.
<div >
<table id="aboutImages">
<tr>
		<td>
			<a href="https://www.utwente.nl/en/" target="_blank"><img class="aboutImage" src="./images/logo_university_of_twente.png" /></a>
		</td>
		<td>
			<a href="http://fmt.cs.utwente.nl/" target="_blank"><img class="aboutImage" src="./images/logo_fmt.png" /></a>
		</td>
</tr>
<tr>
		<td>
			<a href="http://fmt.cs.utwente.nl/tools/animo/" target="_blank"><img class="aboutImage" src="./images/logo_animo.png" /></a>
		</td>
		<td>
			<a href="http://uppaal.com/" target="_blank"><img class="aboutImage" src="./images/logo_up4all.png" /></a>
		</td>
</tr>
</table>
		Version 1.0
	<input style="display:inline; float:right;" value="Close" type="button" id="btn-about-close" >
</div>
	</div>

	<!-- loading popup -->
	<div id="popLoading" class="pagePopup">
		<h1>Loading...</h1>
		<div class="smallMargin"></div>
		please wait...<br />
	</div>

</div>

</body>
</html>
