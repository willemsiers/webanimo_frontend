var inDrag = null;
var showDiff = false;

// WIP!!! 
//name: property name in the graph model. Returns associated input #id
var propertyToInputSelector = function(name, isEdge)
{
	switch(name)
	{
		case "moleculeType" : return '#moleculeType';
		case "canonicalName" : return '#name';
		case "initialConcentration" : return '.inputbox-initialActivity';
		case "levels" : return '.inputbox-totalActivity';
		case "description" : return isEdge ? '#edgeDescription':  '#nodeDescription';
		case "enabled" : return '.inputbox-isEnabled';
		case "plotted" : return '.inputbox-isPlotted';
		case "k" : return '.k';
		case "scenario" : return '#reactionType';
		default: return null;
	}
}


/** Localstorage compatibility ~https://developer.mozilla.org/en-US/docs/Web/API/Storage/LocalStorage */
if (!window.localStorage) {
  window.localStorage = {
    getItem: function (sKey) {
      if (!sKey || !this.hasOwnProperty(sKey)) { return null; }
      return unescape(document.cookie.replace(new RegExp("(?:^|.*;\\s*)" + escape(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*((?:[^;](?!;))*[^;]?).*"), "$1"));
    },
    key: function (nKeyId) {
      return unescape(document.cookie.replace(/\s*\=(?:.(?!;))*$/, "").split(/\s*\=(?:[^;](?!;))*[^;]?;\s*/)[nKeyId]);
    },
    setItem: function (sKey, sValue) {
      if(!sKey) { return; }
      document.cookie = escape(sKey) + "=" + escape(sValue) + "; expires=Tue, 19 Jan 2038 03:14:07 GMT; path=/";
      this.length = document.cookie.match(/\=/g).length;
    },
    length: 0,
    removeItem: function (sKey) {
      if (!sKey || !this.hasOwnProperty(sKey)) { return; }
      document.cookie = escape(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/";
      this.length--;
    },
    hasOwnProperty: function (sKey) {
      return (new RegExp("(?:^|;\\s*)" + escape(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
    }
  };
  window.localStorage.length = (document.cookie.match(/\=/g) || window.localStorage).length;
}

$(document).ready(function() { 

	// cy settings
	cy.maxZoom(10.0);
	cy.minZoom(1.0 / 10.0);

	/** menu zoom shortccuts */
	$('.zoomIn').click(zoomOut);
	$('.zoomOut').click(zoomIn);
	$('.fit').click(zoomFit);

	/** menu undo redo */
	$('.undo').click(restorePreviousState);
	$('.redo').click(restoreNextState);

	/** reoganize reactors toolbar-item */
	$('.reorganize').click(function() { saveCurrentState(); cy.layout({name: 'cose'}); });

	$('.toggleRevisionList').click(function(e){
		if($('#changelogPane').is(':visible'))
		{
			$('.toggleRevisionList').removeClass('toggleRevisionListChecked');
			onToggleShowChangelog(false);
		}
		else {
			$('.toggleRevisionList').addClass('toggleRevisionListChecked');
			onToggleShowChangelog(true);
		}
	});

// bind on a lot
	var bindElements = [document, $('input[type=submit]'), $('input[type=button]'), $('select'), $('canvas'), $('button')];
	for(var el in bindElements) {
		el = bindElements[el];

		// and panning 
		$(el).bind('keydown', 'right', moveRight);
		$(el).bind('keydown', 'left', moveLeft);
		$(el).bind('keydown', 'up', moveUp);
		$(el).bind('keydown', 'down', moveDown);
		$(el).bind('keydown', 'home', zoomFit);


		/** Ctrl Z */
		$(el).bind('keydown', 'ctrl+z', restorePreviousState);
		$(el).bind('keydown', 'ctrl+shift+z', restoreNextState);
		$(el).bind('keydown', 'ctrl+y', restoreNextState);

		/** Zoom shortcuts */
		$(el).bind('keydown', '=', zoomOut); 
		$(el).bind('keydown', '-', zoomIn);

		// screen close/save
		$(el).bind('keydown', 'esc', handleEscapeDown);
		$(el).bind('keydown', 'return', handleEnterPress);
		$(el).bind('keydown', 'del', handleDelPress);
	}


	$('input').bind('keydown', 'esc', handleEscapeDown);
	$('textarea').bind('keydown', 'esc', handleEscapeDown);
	$('#editNode').submit(handleEnterPress);
	$('#editEdge').submit(handleEnterPress);


	/** Make div#cy right clickable menu */
	$('div#cy').jeegoocontext('menu');
	cy.on('cxttapend', publicRightClick);
	cy.on('cxttapend', 'node', nodeRightClick);
	cy.on('cxttapend', 'edge', edgeRightClick);


	// ########################## REMOVING AND ADDING EDGES / NODES ###################################
	$('#editNodeCancel').click(editNodeCancel);
	$('#editEdgeCancel').click(editEdgeCancel);
	$('#editEdgeRemove').click(editEdgeRemove);
	$('#editNodeRemove').click(editNodeRemove);
	$('#editEdgeSave').click(editEdgeSave);

	$('.addNode').click(addNodeOpen);
	$('#addNodeSave').click(addNodeSave);
	$('.addEdge').click(openEdgeEdit);
	$('#addEdgeSave').click(addEdgeSave);

	$('#addNodeEdge').click(addNodeEdge);
	$('#deleteNode').click(deleteNode);
	$('#deleteEdge').click(deleteEdge);

	// cy selections
	cy.on('tap', closeSelection);
	cy.on('tap', 'node', openSelectionNode);
	cy.on('tap', 'edge', openSelectionEdge);

	// manipulate cy
	cy.on('tap', 'edge', editEdge);
	cy.on('tap', 'node', editNode);
	cy.on('free', 'node', saveCurrentState);
	
	/** DRAG EDGE */
	cy.on('tapdrag', adjustArrow);
	cy.on('pan', adjustArrow);
	cy.on('tap', closeArrow);

	// ########################## DYNAMIC SAVE IN EDITS ###################################
	
	/** if change something in node edit */
	var changeNode = false;
	$('.nodeListener').change(function() {
		if ($('#editNodeLabel').html() == "Edit reactant") {
			editNodeSave();
		}});
	$('input.node').on('input', function() {
		changeNode = true; 
	});
	$('input.node').on('focusout', function() {
		if ($('#editNodeLabel').html() == "Edit reactant" && changeNode) {
			editNodeSave(); 
			changeNode = false;
		}
	});

	/** if change something in edge edit */
	var changeEdge = false;
	$('.edgeListener').change(function() {
		if ($('#editEdgeLabel').html() == "Edit interaction") {
			editEdgeSave();
		}
	});
	$('input.edge').on('input', function() { changeEdge = true; });
	$('input.edge').on('focusout', function() {
		if ($('#editEdgeLabel').html() == "Edit interaction" && changeEdge) {
			editEdgeSave(); 
			changeEdge = false;
		}
	});
	
	$('#hideAlpaca').click(function() {
		$('#alpaca').hide()
	});

	// ########################## EDIT WINDOWS ###################################

	/** Sets the correct reaction label when reaction type changes */
	$('#reactionType').change(function() {
		hideReactionLabels();
		$('#reactionLabel' + $('#reactionType').val()).show();
		if ($('#editEdgeLabel').html() == "Edit edge") {editEdgeSave() }
	});

	/** Changes the slider inputs */
	$('#totalActivity').on('input', function() {
		var amount = $('#totalActivity').val();
		$('#initialActivity').attr('max', amount);
		$('#initialLabel').html($('#initialActivity').val());
		$('#levelLabel').html(amount);
	});

	$('#initialActivity').on('input', function() {
		$('#initialLabel').html($('#initialActivity').val());
	});


	// ########################## ANALYSE AND EXPORT SHIT ###################################

	/** Toolbar shit */
	$('#doAnalyse').click(analyse);
	$('.cancelPopPage').click(hidePagePopup);
	document.getElementById('importNetwork').addEventListener('change', handleFileSelect, false);

	//disable default ctrl+o and ctrl+s behaviour
	document.onkeydown = function(event){
		var keychar = String.fromCharCode(event.keyCode).toLowerCase();
		
		if ((keychar == 'o' || keychar == 's') && (event.ctrlKey)){
			console.log('prevented ctrl o or s');

			event.cancelBubble = true;
			event.returnValue = false;
			event.keyCode = false;
			return false;
		}
	}

	// ctrl+O shortcut and toolbar binding
	var importFunction = function() {
		$('#importNetwork').trigger('click');
	};
	$('#btn-about-close').click(hidePagePopup);
	$('#newNetworkButton').click(clearNetwork);
	$('#importNetworkButton').click(importFunction);
	$(document).bind('keydown', 'ctrl+o', importFunction);
	
	/* button enable/disable effect on plot */
	$('#enabled').change(function() {if ($('#editNodeLabel').html() == "Edit reactant") {editNodeSave(); } setPlotHidden()});
	$('#disabled').change(function() {if ($('#editNodeLabel').html() == "Edit reactant") {editNodeSave();} setPlotHidden()});
	
	$("#k_slider").change(function() {
		var value = document.getElementById("k_slider").value;
		document.getElementById("k").value = Math.pow(2,value) * 0.001;
		editEdgeSave();
	});

	$("#exportGraph").click(exportFunction);
	$(document).bind('keydown', 'ctrl+s', exportFunction);
	
    $("#example_chrondrocyte").click(function() {loadJson(jQuery.extend(true, {}, ex_chondrocyte));saveToLocalStorage();});
    $("#example_modelbase").click(function() {loadJson(jQuery.extend(true, {}, ex_modelbase));saveToLocalStorage();});
    $("#example_echo").click(function() {loadJson(jQuery.extend(true, {}, ex_echo));saveToLocalStorage();});
    
    $("#exportGraphImage").click(exportGraphImage);
    $("#getShareableLink").click(getShareableLink);
    $("#about").click(showPopupAbout);

	/** menu sjit */
	$('#mainMenu > li').hover(function(e) {
		var $el = $('ul', this);
		if ($('#mainMenu > li > ul:visible').length) {
			$('#mainMenu > li > ul').not($el).hide();
			$el.stop(true, true).show();
		}
		return false;
	});
	
	$('#mainMenu > li').click(function(e) { // limit click to children of mainmenue
        var $el = $('ul',this); // element to toggle
        $('#mainMenu > li > ul').not($el).hide(); // slide up other elements
        $el.stop(true, true).toggle(); // toggle element
        return false;
    });
    $('#mainMenu > li > ul > li > a').click(function(e) {
        e.stopPropagation();  // stop events from bubbling from sub menu clicks
		$('#mainMenu > li > ul').hide();
		$('#menu').hide();
    });
    $(document).on('click','*',function(e){
        e.stopPropagation();
        $('#mainMenu > li > ul').hide();
		$('#menu').hide();
    });
	
	/** tooltips */
	$('#toolbar > a').qtip({ 
		position: {
			my: 'top center',  
			at: 'bottom center',
            viewport: $(window)
		}
	});
	
	/** resizable editor */
	$('#webANIMOEditor').resizable({
		handles: 'e',	
	});
	
	/** on resize */
	$('#webANIMOEditor').resize(function() {
		// width
		var calcWidth = $('.contentWrapper').width() - $('#webANIMOEditor').width();
		$('#editorResize').width($('#webANIMOEditor').width());
		$('#resultResize').width(Math.floor(calcWidth) - 1);
		cy.resize();
	});
	
	/** on button clicks resizes */
	$('.slideLeft').click(function() {
		$('#webANIMOEditor').width(10);
		$('#resultResize').width($('.contentWrapper').width()-10);
		$('#webANIMOEditor').trigger('resize');
	});
	
	$('.slideRight').click(function() {
		$('#webANIMOEditor').width($('.contentWrapper').width());
		$('#resultResize').width(0);
		$('#webANIMOEditor').trigger('resize');
		cy.fit();
	});
	
	$('.slideReset').click(function() {
		var halfWidth = $('.contentWrapper').width() / 2;
		$('#webANIMOEditor').width(halfWidth);
		$('#resultResize').width(halfWidth);
		$('#webANIMOEditor').trigger('resize');
		cy.fit();
	});

	existUnsavedChanges = false;
	if(window.loadedNetwork !== null)
	{
		$('#saveToCloudStatus').text('Loaded network: '+window.loadedNetworkId );
		$('#saveToCloudStatus').show();
		
		//occasionally send heartbeat to server
		setInterval(function() {

			var save_script_url = './heartbeat.php?network='+loadedNetworkId;
			$.post(save_script_url, {}).success(function(received_data){
				//console.log('heartbeat success', received_data);
			}).
			fail(function(e){
				console.log('heartbeat failed!', received_data);
				displayConsoleMessage("Lost connection to server... Error:" + decoded.message, 'log-error', 7000);
				$('#saveToCloudStatus').text('Server connection lost');
				var decoded = JSON.parse(e.responseText);
			});
		}, 5000);
	}

	/** Load network from previous visit out of localStorage */
	if (	(window.loadedNetwork == null && !window.lockError) 
			&& typeof(window.localStorage) !== 'undefined' 
			&& window.localStorage.hasOwnProperty('animo-save')) { 
		var loadedGraph = JSON.parse(window.localStorage.getItem('animo-save')).model;
		loadJson(loadedGraph);
		console.log('Restored save from localStorage');
	} else if (window.loadedNetwork != null || window.lockError) {
		loadJson(window.loadedNetwork.model);
		displayConsoleMessage('Loaded shared network from online storage');
		if (lockError) {
			$('.lockerror').show();
			displayConsoleMessage('Someone else has already locked the network, cannot save changes online', 60);
		}
	}

	saveToLocalStorage = function()
	{
		var newExport = exportJSON();
		if (newExport.localeCompare(window.previousExport) != 0) {
			console.log('Detected changes, saved in localStorage');
			window.localStorage.setItem('animo-save', newExport);
			window.previousExport = newExport;
		}
		existUnsavedChanges = false;
	}

	saveToCloud = function()
	{
		console.log('Saving to the cloud');

		var save_script_url = './savenetwork.php?network='+loadedNetworkId;
		var network_json = exportJSON();
		window.previousExport = network_json;
		var data = {"network":network_json};
		$.post(save_script_url, data).success(function(received_data){
			console.log('success', received_data);
			displayConsoleMessage("Saved network!", 'log-success');
			received_data = JSON.parse(received_data);
			var date = new Date();
			var dateString = date.getHours() +':'+ date.getMinutes() +':'+ date.getSeconds();
			$('#saveToCloudStatus').text('All changes saved to the cloud ('+dateString+')');
			existUnsavedChanges = false;
		}).
		fail(function(e){
			$('#saveToCloudStatus').text('Error saving to the cloud');
			var decoded = JSON.parse(e.responseText);
			displayConsoleMessage("Request failed: " + decoded.message, 'log-error', 10000);
		});
	}

	// periodically save in localstorage
	if (typeof(window.localStorage) !== 'undefined') {
		var saveInterval = 1000; // save every five seconds to localStorage (if changed)
		window.previousExport = window.localStorage.hasOwnProperty('animo-save') ? window.localStorage.getItem('animo-save') : '';
		setInterval(function() {
			if(!showDiff && existUnsavedChanges === true)
			{
				if(loadedNetwork) //TODO: is 'laodedNetwork' cleared on new network?
				{
					saveToCloud();
				}else
				{
					console.log('Saving to local storage');
					saveToLocalStorage();
				}
			}else{
				existUnsavedChanges = false;
			}
			}, 
			saveInterval);
	}

}); 
