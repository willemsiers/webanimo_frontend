projectLog = [];

var getNodeChanges = function(node, revisionChanges)
{
	return revisionChanges[node.data('id')];
	/**
	console.log('node:::',node);
	var nodeName = node.data('canonicalName');
	$.each(revisionChanges, function(name, change)
			{
				if(name === 'canonicalName')
				{
					if(change.original_value === nodeName)
					{
					}else
					{
					}
				}
			});
	 **/
}

var getRevisionChanges = function(revision){
	revision = revision || getRevision();
	return (revision && (!$.isEmptyObject(revision))) ? revision.changes : {};
}

var getRevision = function(){
	return projectLog[projectLog.length -1];
}

var addLogDeleted = function(node, group)
{
	if(group === 'nodes' || group === 'edges')
	{
		console.log('group', group, node);
		var revisionChanges  = getRevisionChanges();
		var removed = revisionChanges.removed || {};
		var groupRemoved = removed[group] || {};
		groupRemoved [node.data('id')] = node.data();

		removed[group] = groupRemoved;
		revisionChanges.removed = removed;
		projectLog.changes = revisionChanges;
		if(!showDiff)
		{
			$('#saveToCloudStatus').text('Saving changes to the cloud...');
			existUnsavedChanges = true;
		}
	}else
	{
		console.error('Group must be nodes or edges.');
	}
}

var addLogEntry = function(node, field_name, new_value)
{
	var revisionChanges  = getRevisionChanges();
	var nodeChanges = revisionChanges[node.data('id')] || {};
	var entry = nodeChanges && nodeChanges [field_name]; 
	if(!entry)
	{
		entry = {
			'original_value':node.data(field_name), 
			'new_value':new_value
		}
	}else
	{
		entry.new_value = new_value;
	}

	if(entry.new_value === entry.original_value)
	{
		delete nodeChanges[field_name];
	}else{
		nodeChanges[field_name] = entry;
	}

	if( (!nodeChanges) || (! $.isEmptyObject(nodeChanges)))
	{
		delete revisionChanges[node.data('id')];
	}

	
	revisionChanges[node.data('id')] = nodeChanges;
	if(!showDiff)
	{
		existUnsavedChanges = true;
		$('#saveToCloudStatus').text('Saving changes to the cloud...');
	}
	return projectLog[projectLog.length -1].changes = revisionChanges;
}

var printProjectLog = function()
{
	console.log('project log: ');
	$(projectLog).each(function(i,entry){
		console.log('Rev. #'+entry.revision);
		$(entry.changes).each(function(i,el){
			console.log( '\t' , $.isEmptyObject( el ) ? 'Empty' : el );
		});
	});
}

var mapOnGraph = function( func )
{
	var nodes = cy.nodes();
	var edges = cy.edges();
	$(nodes).each(function(i,node){
		func(node);
	});
	$(edges).each(function(i,edge){
		func(edge);
	});
}

var infToBio = {
	initialConcentration: "Initial activity level",
	activityRatio: "Activity",
	levels: "Total activity levels",
	canonicalName: "Name",
	edges: "Edges",
	nodes: "Nodes",
	k: "K factor",
	"_REACTANT_ACT_E1": "E1 active",
	"_REACTANT_ACT_E2": "E2 active",
	"_REACTANT_E1": "E1 reactant",
	"_REACTANT_E2": "E2 reactant"

};

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

var changeLogEntryToString = function(name,change)
{
	var result = '';
	if(name !== 'undefined')
	{
		if($.isEmptyObject( change) )
		{
			result =  'No Changes';
		}else
		{
			result += '<b>'+name+'</b>';
			$.each(change,function(type, vals){
				//console.log(type);
				if(type === 'activityRatio')
				{
					return ""; //a.k.a. continue
				}
				result += '&#013;&#010;';

				var newType = infToBio[type];
				//BOBE DOE NORMAAL
				if (typeof(newType) == "string") {
					result += newType;
				}
				else {
					result += capitalizeFirstLetter(type);
				}

				if(type !== 'created')
				{
					result += ': ';
				}
				if(typeof(vals.original_value) !== 'undefined')
				{
					result += vals.original_value; 
					result += '\u2192';
					result += vals.new_value; 
				} else if(type === 'edges' || type === 'nodes')
				{
					$.each(vals, function(name, val)
							{
								result += name;
								result += ' ';
							});
				} else if(typeof(vals.original_value) !== 'undefined')
				{
					result += vals.new_value; 
				}
				result += '</br>';
			});
		}
	}
	return result;
}

var hideDiffAndRestoreCurrent = function()
{
	selectedRevision = null;
	cy.elements('node.has-changes').css({ });
	cy.elements('node.no-changes').css({ });
	cy.elements('node[added]').css({ });
	cy.elements('node[removed]').css({ });
	cy.elements('edge.has-changes').css({ });
	cy.elements('edge.no-changes').css({ });
	cy.elements('edge[added]').css({ });
	cy.elements('edge[removed]').css({ });
	showDiff = false;
	cy.load();
	cy.add(getRevision().finalGraph);	

	$('.inputBox').each( function(i, inputbox){
		$(inputbox).find('input,select,textarea,div').each(function(j, input){
			$(input).prop('disabled',false );
			$(input).removeClass('diff-input-changed');
		});
	});
	$('input').removeClass('diff-input-changed');
}

var showDiffFromRevision = function(revision)
{
	var diffBaseGraph = revision.finalGraph;
	showDiff = true;
	cy.load();
	cy.add(diffBaseGraph);
	if(revision.changes.removed)
	{
		$.each(revision.changes.removed['nodes'], function(name,el){
			console.log('el',el);
			el.removed = true;
			cy.add( {
				'data':el,
				'position': {'x':el.Position_X, 'y':el.Position_Y}
			});
		});
		$.each(revision.changes.removed['edges'], function(name,el){
			el.removed = true;
			cy.add( {
				'data':el
			});
		});
	}

	$.each(revision.changes, function(id, change){
		if(change.created)
		{
			element = cy.getElementById(id);
			element.data('added',true);
		}
	});

	//Styling of graph
	var diff_added 	 	= "#10E010";
	var diff_removed 	= "#E01010";
	var diff_changed 	= "#10F0F0";
	var diff_grey 		= "#C0C0C0";

	//saveCurrentState(); // create ctrl+z snapshot
	zoomFit(); // fit graph

	mapOnGraph(function(el){
		highlightDiffInputs(revision, el);
	});

	cy.elements('node.no-changes').css({
		"background-color" : diff_grey
	});
	//"border-style" : "dotted",
	cy.elements('edge.no-changes').css({
		"line-color" : diff_grey
	});
	cy.elements('edge.has-changes').css({
		"line-color" : diff_changed,
		"background-color" : diff_grey
	});
	cy.elements('node.has-changes').css({
		"border-color" : diff_changed,
		"background-color" : diff_grey
	});
	cy.elements('node[removed]').css({
		"border-color" : diff_removed,
		"background-color" : diff_grey
	});
	cy.elements('edge[removed]').css({
		"line-color" : diff_removed,
	});
	cy.elements('node[added]').css({
		"border-color" : diff_added,
		"background-color" : diff_grey
	});
	cy.elements('edge[added]').css({
		"line-color" : diff_added,
	});

	$('.inputBox').each( function(i, inputbox){
		$(inputbox).find('input,select,textarea').each(function(j, input){
			$(input).prop('disabled',true );
		});
	});
}

var clearProjectLog = function()
{
	projectLog = [];
	projectLog.push( { 'revision':1, 'changes': {} , 'finalGraph': {'nodes':[], 'edges':[]}});
	$('#changelogPane').hide();
	$('#resultPane').show();
	$('.toggleRevisionList').removeClass('toggleRevisionListChecked');
}

var onToggleShowChangelog = function(val)
{
	var checked = val;
	var $pane = $('#changelogPane');
	var $list = $('#changelogList');
	$list.children('').remove();
	if(checked)
	{
		$(projectLog).each(function(i,entry){
			$revision = $('<button class="changelog-revision"></button>');
			$revision.append( $( '<h class="changelog-header">Revision #'+entry.revision
						+ (i === (projectLog.length-1) ? ' (current)' : '')
						+ '</h>') );
			$.each(entry.changes,function(name,el){
				$revision.append( '<li class="changelog-change">'+changeLogEntryToString(name,el)+'</li>');
			});
			$list.prepend($revision);
			$revision.click(entry, loadRevision);
		});
		$('#resultPane').hide();
		$('.toggleRevisionList').addClass('toggleRevisionListChecked');

		// disable left bar
		$('.fadeMeOutshizzle').show();
		 $('#leftBar').css('overflow-y', 'hidden');


		$pane.show();
	}else{


		// disable left bar
		$('.fadeMeOutshizzle').hide();
		$('#leftBar').css('overflow-y', 'scroll');

		hideDiffAndRestoreCurrent();
		$('#changelogPane').hide();
		$('#resultPane').show();
		$('.toggleRevisionList').removeClass('toggleRevisionListChecked');
	}
}

var loadRevision = function(e)
{
	var revision = e.data;
	selectedRevision = revision;
	showDiffFromRevision(revision);
}
clearProjectLog();
