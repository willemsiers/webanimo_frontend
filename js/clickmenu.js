/** Canvas menu */
var publicRightClick = function(e) {
	$('#deleteNodeMenu').hide();
	$('#deleteEdgeMenu').hide();
	$('#posX').val(e.cyRenderedPosition.x);
	$('#posY').val(e.cyRenderedPosition.y);
	$('#addMenu').show();
};

var nodeRightClick = function(e) {
	var node = e.cyTarget;
	$('#addMenu').hide();
	$('#deleteNodeMenu').show();
	$('#nodeId').val(node.data("id"));
};

var edgeRightClick = function(e) {
	var edge = e.cyTarget;
	$('#addMenu').hide();
	$('#deleteEdgeMenu').show();
	$('#edgeId').val(edge.data("id"));
};