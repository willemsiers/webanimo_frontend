var displayConsoleMessage = function(text, typeClass, durationMs)
{
	console.log("!!", text);
	var duration = durationMs || 3000;

	var newEl = $("<li class=\"consoleLogMessage\"></li>");
	if(typeClass)
	{
		newEl.addClass(typeClass);
	}
	newEl.text(text);

	var el = $('#toast-container');
	el.prepend(newEl)

	setTimeout( function(){
		newEl.animate({'height':'0px',
			'padding-top': '0px',
			'padding-bottom': '0px',
			'opacity':'0',
			'margin-left':'-100px'
		},
				300, 
				'swing',
				function()
				{
					newEl.remove();
				});
	}, duration);
}

var printStackTrace = function()
{
	var e = new Error('dummy');
	var stack = e.stack.replace(/^[^\(]+?[\n$]/gm, '')
		.replace(/^\s+at\s+/gm, '')
		.replace(/^Object.<anonymous>\s*\(/gm, '{anonymous}()@')
		.split('\n');
	console.log(stack);
}
