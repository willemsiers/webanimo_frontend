function b64toBlob(b64Data, contentType, sliceSize) {
	contentType = contentType || '';
	sliceSize = sliceSize || 512;

	var byteCharacters = atob(b64Data);
	var byteArrays = [];

	for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
		var slice = byteCharacters.slice(offset, offset + sliceSize);

		var byteNumbers = new Array(slice.length);
		for (var i = 0; i < slice.length; i++) {
			byteNumbers[i] = slice.charCodeAt(i);
		}

		var byteArray = new Uint8Array(byteNumbers);

		byteArrays.push(byteArray);
	}

	var blob = new Blob(byteArrays, {type: contentType});
	return blob;
}

n3Charts.Factory.Transition.defaultDuration = 40;
var graphCtrlApp = angular
.module('graphCtrlApp',['n3-line-chart'])
.controller('GraphCtrl', function( $scope ) {

	var calculateChartDimensions = function()
	{
		var boundingRect = d3.select('g.grid').node().getBoundingClientRect();
		var chartDimensions = {
			x1 : boundingRect.left,
			y1 : boundingRect.top,
			x2 : boundingRect.right,
			y2 : boundingRect.bottom
		}
		chartDimensions.width = chartDimensions.x2-chartDimensions.x1;
		chartDimensions.height = chartDimensions.y2-chartDimensions.y1;
		$scope.chartDimensions = chartDimensions;
		$scope.$apply();
	}

	var onChartReady = function()
	{
		var resizeTimeoutId; 
		angular.element($(window)).bind('resize', function()
				{
					clearTimeout(resizeTimeoutId);
					resizeTimeoutId = setTimeout(function(){
						/**
						 * Get dimensions
						 */
						calculateChartDimensions();
					}, 250); //timeout should be (much) higher than Transition.defaultDuration
					$scope.$digest();
				});
		$scope.$watch('chartDimensions', function(newValue)
				{
					updateAxes(newValue);
				});
		calculateChartDimensions();
	};

	var vm = this;
	vm.selectedTime = 0.0;
	vm.network = '';
	$scope.data = {'dataset0':[], 'dataset1':[]};
	$('svg.chart').ready(function(e){
		onChartReady();
	});

	$scope.$watch("vm.network", function(newValue, oldValue) {
		if (vm.network == ''){
			return;
		}

		loadNetworkFromJSON(vm.network);
	});

	//Todo: "enabled" is actually "disabled"
	$scope.$watch("vm.xPanEnabled", function(oldValue, newValue){
		if($scope.options){
			$scope.options.pan.x = ! newValue;
		}
	});
	$scope.$watch("vm.yPanEnabled", function(oldValue, newValue){
		if($scope.options){
			$scope.options.pan.y = ! newValue;
		}
	});

	vm.setSelectedInitialActivitiesOnNetwork = function( )
	{
		var ids = Object.keys($scope.data);

		var nodes = cy.nodes();
		cy.startBatch();
		ids.forEach(function(id){
			var node = cy.getElementById(id.substr(1));
			var selectedActivity = node.data('activityRatio');

			var levels = node.data('levels');

			//Server only accepts integers
			var newInitialConcentration = Math.round(selectedActivity*levels); 
			node.data('initialConcentration', newInitialConcentration );
		});

		cy.endBatch();
		displayConsoleMessage("Concentrations set on current network");
	};

	/**
	 * download: Download image if true, otherwise open in browser
	 */
	vm.exportImage = function( download )
	{
		displayConsoleMessage("Exporting graph Image");

		// Get SVG element from DOM and clone it so we don't wreck the original plot
		var svg = $('svg.chart').clone();
		// XMLNS is required for svg's
        svg.attr('xmlns', 'http://www.w3.org/2000/svg');

		// Increase width and height of SVG sizes
        var svgWidth = parseInt(svg.attr('width'), 10) + 20 + 200; // 20 for activity label space, 200 for legend
        var svgHeight = parseInt(svg.attr('height'), 10) + 50; // 50 for time label space
		
		// Set the new sizes
		svg.attr('height', svgHeight);
		svg.attr('width', svgWidth);
       
		// Remove extra grid numbers
		var candidates = svg.find(".y-grid").find(".tick");
        for (var i = 0; i < candidates.length; i++) {
            candidates[i].remove();
        }

		// Remove top bar (and the text above that bar)
        var candidates = svg.find(".axis.x2-axis");
        for (var i = 0; i < candidates.length; i++) {
            candidates[i].remove();
        }
		
		// Remove scrollbar
		svg.find(".scroll-line-container").remove();

		// Resize and move container to give the activity label more space
		// and make the time label visible by moving it down
        svg.find("g.container").attr('height', 500);
        svg.find("g.container").attr('transform', 'translate(60, 20)');
		// Move down the x label 28 units
        svg.find('text.x-label').attr('y', parseInt(svg.find("text.x-label").attr('y'), 10) + 28);

		// Adding of the legend happens here
		// Get all currently visible series
		var visibleSeries = [];
		if ($scope.options != undefined
				&& $scope.options != null
				&& $scope.options.series != null
				&& $scope.options.series != undefined) {
			visibleSeries = $scope.options.series.filter(function(s) {
				return s.visible
			});
		}

		
		// Create a main legend container element 200 pixels left of the right
		// border of the image
		var textContainer = $(document.createElement('g'));
		textContainer.attr('transform', 'translate(' + (svgWidth - 200) + ',0)');
		svg.append(textContainer);

		// For each visible series, add a colored rectangle and its name 
		// to the legend container
		visibleSeries.forEach(function(v, i) {
			var g = $(document.createElement('g'));
			var y = 100 + i * 30;
			g.attr('transform', 'translate(0,' + y + ')');
			textContainer.append(g);

			var text = $(document.createElement('text'));
			text.html(v.label);
			text.attr('transform', 'translate(20, 0)');
			text.attr('font-size', '15');
			g.append(text);

			var color = $(document.createElement('rect'));
			color.attr('style', 'fill: ' + v.color);
			color.attr('width', '15');
			color.attr('height', '15');
			color.attr('transform', 'translate(0, -12)');
			g.append(color);
		});

		// Set all texts to the default a sans-serif font
		svg.find("text").attr("font-family", "sans-serif");

		// Save the final SVG data as text
		var svgData = svg.prop('outerHTML');

		// Get the canvas element (both with and without jquery helper functions)
        var canvas = document.getElementById('canvas-export-graph');
        var $canvas = $('#canvas-export-graph');
        $canvas.attr('width', svgWidth);
        $canvas.attr('height',svgHeight);

		// Clean the canvas
        var ctx = canvas.getContext('2d');
        ctx.fillStyle = "rgba(255, 255, 255, 255)";
        ctx.fillRect(0,0,canvas.width,canvas.height);

        var svg = new Blob([svgData], {type: 'image/svg+xml;charset=utf-8'});
		// Turn SVG into a browser file resource
        var DOMURL = window.URL || window.webkitURL || window;
        var url = DOMURL.createObjectURL(svg);

        var img = new Image();
        img.onload = function () {
			// Draw the image and convert it to dataurl
            ctx.drawImage(img, 0, 0);
            var imageDataUrl = canvas.toDataURL('image/png');

            if(download) {
				// Save the plot
                displayConsoleMessage("Downloading Image");
                var b64Data = imageDataUrl.substr( imageDataUrl.indexOf(',')+1, imageDataUrl.length);

                var blob = b64toBlob(b64Data, 'image/png');

                saveAs(blob, "ANIMO_graph.png");
            } else {
				// Open the plot in a new window
                var a = document.createElement('a');
                a.target = "_blank";
                a.href = imageDataUrl;
                a.style.visibility = 'hidden';
                document.body.appendChild(a);
                $(a)[0].click();
                displayConsoleMessage("Make sure that you allow pop-ups to be opened")
			}

			//Cleanup file resource
			DOMURL.revokeObjectURL(url);
		}
		
		// Assing the URL to the image to trigger the onload
		// This has to be done to force loading of the url object before execution of the drawImage code
		img.src = url;
	};

	var loadNetworkFromJSON = function( network_json)
	{
		$('#popPage').show();
		$('#popPageContent').show();
		$('#popLoading').show();
		var data_url = './processing.php';
		$.post(data_url, {'network': network_json}).success(function( received_data){
			hidePagePopup();

			displayConsoleMessage("Request successful", 'log-success');

			received_data = JSON.parse(received_data);
			var dataset_ids = Object.keys(received_data);

			var series = [];
			var colors = [ [255, 1, 1], [1, 255, 1],[1, 1, 255]];
			var a = 81; //delta in color map
			var colorIndex = 0;
			dataset_ids.forEach(function(id)
					{
						var node = cy.getElementById(id.substr(1));

						var canonicalName = node.data('canonicalName');

						//formats a number as hex, padded with zeroes to 2 digits
						var colorFormat = function(a) 
						{
							hex = a.toString(16);
							return hex.length >= 2 ? hex : '0'+hex;
						}

						var color = colors[colorIndex];
						var serie = {
							axis: "y",
							dataset: id,
							key: "y",
							label: canonicalName,
							color: '#'+colorFormat(color[0]) + colorFormat(color[1]) +colorFormat(color[2]),
							type: ['line' ], //'area' , 'dot' 
							id: id,
							visible: node.data('enabled') && node.data('plotted'),

						};
						colorIndex = (colorIndex + 1) % colors.length;
						if(colorIndex == 0)
						{
							colors = colors.map(function (c, i)
									{
										c[i] -= a;
										c[(i+1) % 3] += a;
										return c.map(function(x) { return Math.abs(x%256) });
									});
						}

						series.push(serie);
					});


			//find maximum Y value in data sets
			var yMax = Object.keys(received_data).
				reduce( function(global_max, setName) {
					var set = received_data[setName];
					return set.reduce( function(local_max, pos) {
						return Math.max(pos.y, local_max);
					}, global_max);
				}, 0);

			yMax = yMax + 1; //padding

			var options = {
				series: series,
				pan : {
					x : false,
					y : false
				},
				axes: {
					x: {
						key: "x",
						min: 0,
					},
					y: {
						key: "y",
						max: yMax,
						min: 0
					}
				}
			};

            // Plot zooming with keybindings, buttons & scroll
            var plotGlobalZoomScale = 1;

            var d3ZoomObj = d3.behavior.zoom();

			d3.select('.chart').call(d3ZoomObj.on("zoom", function (e) {
				var zoom = d3.event.scale;
				var translation = d3.event.translate;
				//snap zoom to 1, and enable pan if zoom != 1
				zoom = zoom > 0.95 && zoom < 1.05 ? 1 : zoom;
				//$scope.options.pan.x = (zoom !== 1);
                
                plotGlobalZoomScale = d3.event.scale;

                // Clamp the zoom of the plot
                plotGlobalZoomScale = Math.max(1, plotGlobalZoomScale);

                d3ZoomObj.scale(plotGlobalZoomScale);

				vm.selectedSimulationEndTime = Math.min(vm.simulationEndTime * (1/plotGlobalZoomScale),vm.simulationEndTime);
				$scope.$apply();
			}));

            var zoomPlotIn = function() {
                plotGlobalZoomScale *= 1.1;
                vm.selectedSimulationEndTime = Math.min(vm.simulationEndTime * (1/plotGlobalZoomScale),vm.simulationEndTime);
                $scope.$apply();
            }

            var zoomPlotOut = function() {

                plotGlobalZoomScale *= 0.9;
                plotGlobalZoomScale = Math.max(1, plotGlobalZoomScale);

                vm.selectedSimulationEndTime = Math.min(vm.simulationEndTime * (1/plotGlobalZoomScale),vm.simulationEndTime);
                $scope.$apply();
            }

            /** Plot zoom shortcuts */
            $(document).bind('keydown', 'shift+=', zoomPlotIn);
            $(document).bind('keydown', 'shift+-', zoomPlotOut);

            /** Plot zoom buttons */
            $('.zoomOutPlot').click(function() {
                zoomPlotOut();
            });

            $('.zoomInPlot').click(function() {
                zoomPlotIn();
            });

			//todo: error handling?

			vm.simulationEndTime = Object.keys(received_data).
				reduce( function(global_max, setName) {
					var set = received_data[setName];
					var local_max = set[set.length-1].x;
					return Math.max(global_max, local_max);
				}, 0);

			$scope.data = received_data;
			$scope.options = options;

			$scope.$apply();
			d3.select('.chart-legend').attr('transform', 'translate(100,400)').style('position','relative');

			if($('.scroll-line-container').length == 0){
				//todo: resizing
				addScrollbar($scope.chartDimensions);
			}
		}).
		fail(function(e){
			var decoded = JSON.parse(e.responseText);
			displayConsoleMessage("Request failed: " + decoded.message, 'log-error', 10000);
			$scope.data = {};
			$scope.options = {'data':[]};
			vm.simulationEndTime = 10;
			$scope.$apply();
			hidePagePopup();
		});
	};

	function handleCsvSelect(evt) {
		var file = evt.target.files[0]; // FileList object

		var reader = new FileReader();

		reader.onload = function(evt) {
			var csvString = evt.target.result;
			var csv = Papa.parse(csvString);
			var rows = csv.data;

			var series = [];
			var datas = {};

			for (var x = 1; x < rows[0].length - 1; x++) {
				var id = csv.data[0][x];

				var serie = {
					axis: "y",
					dataset: id,
					key: "y",
					label: id,
					color: "#42B0B3",
					type: ['line'],
					id: id,
					visible: true
				};

				var data = [];

				for (var y = 1; y < rows.length; y++) {
					if (rows[y].length <= 1) continue;

					data.push({
						x: rows[y][0],
						y: rows[y][x]
					});
				}

				datas[id] = data;
				series.push(serie);
			}

			if (!('options' in $scope)) {
				$scope.options = {
					series: series,
					pan : {
						x : false,
						y : false
					},
					axes: {
						x: {
							key: "x",
							min: 0,
						},
						y: {
							key: "y",
							max: 100,
							min: 0
						}
					}
				};

				$scope.data = datas;
			} else {
				for (var attrname in datas) { $scope.data[attrname] = datas[attrname]; }
				for (var attrname in series) { $scope.options.series.push(series[attrname]); }
			}

			$scope.$apply();

			$('#mainMenu > li > ul').hide();

			$('#importCsvForm')[0].reset();
		}

		reader.readAsText(file);
	}

	document.getElementById('importCsv').addEventListener('change', handleCsvSelect, false);

	vm.importCSV = function() {
		$('#importCsv').trigger('click');
	}

	vm.downloadCSV = function()
	{
		var csv = exportCSV();
		saveAs(new Blob([csv]), "ANIMO_data.csv");
	}

	/**
	 * Converts the current $scope.data to a csv string
	 * Returns csv string
	 */
	var exportCSV = function ( )
	{
		displayConsoleMessage("Generating CSV...");
		//https://tools.ietf.org/html/rfc4180
		//minus same length records
		var csv = "";
		var data = $scope.data;

		var ids = Object.keys(data);

		ids = ids.filter(function(id, index, ids) {
			var node = cy.getElementById(id.substr(1));
			return node.data("plotted");
		});

		points = [];

		ids.forEach(function(id, i){
			var serie = data[id];

			if(serie.length > 0)
			{
				serie.forEach(function(datapoint, j)
						{
							points.push({
								id: id,
								x: datapoint.x,
								y: datapoint.y
							});
						});
			}
		});

		points.sort(function(p1, p2) {
			return p1.x - p2.x;
		});

		csv += "Time (min),";
		ids.forEach(function(id, i) {
			var node = cy.getElementById(id.substr(1));
			var name = node.data('canonicalName');

			csv += name;
			csv += ",";
		});
		csv += "number_of_levels,\n";

		var hasAppendedLevels = false;

		function appendCsvLine(timePoints) {
			csv += timePoints[0].x;
			csv += ",";
			ids.forEach(function(id, i) {
				p = null;

				timePoints.forEach(function(tp, j) {
					if(tp.id === id) {
						p = tp;
					}
				});

				if(p != null) {
					csv += p.y;
				} else {
					csv += " ";
				}

				csv += ",";
			});

			if(!hasAppendedLevels) {
				var node = cy.getElementById(timePoints[0].id.substr(1));
				var levels = node.data("levels");

				csv += levels;
				csv += ","

					hasAppendedLevels = true;
			}

			csv += "\n";
		}

		prevX = 0;
		currentTimePoints = [];
		points.forEach(function(point, i) {
			if(point.x != prevX) {
				prevX = point.x;

				appendCsvLine(currentTimePoints);

				currentTimePoints = [];
			}

			currentTimePoints.push(point);
		});

		appendCsvLine(currentTimePoints);

		return csv;
	}


	/**
	 * dataset: array of {x, y} objects
	 */
	var getYForX = function(dataset, x)
	{
		if(dataset.length > 1)
		{
			var minValueIndex;
			$.each(dataset, function(index, value) {
				minValueIndex = index;
				return value.x < x;
			});
			minValueIndex--; // keke
			//var minValueIndex = dataset.findIndex(function(el){
				//return el.x >= x;
			//})-1;
			minValueIndex = Math.max(0, minValueIndex);
			var maxValueIndex = minValueIndex + 1;

			var intervalWidth = dataset[maxValueIndex].x - dataset[minValueIndex].x;
			var lerp = (x - dataset[minValueIndex].x) / intervalWidth;

			var y;
			if( typeof (dataset[maxValueIndex].y) != 'undefined') //use either 'y' or 'y1' property
			{
				var intervalYDiff = dataset[maxValueIndex].y - dataset[minValueIndex].y;
				// Use linear interpolation
				y = dataset[minValueIndex].y + intervalYDiff * lerp;
			}
			else
			{
				var intervalYDiff = dataset[maxValueIndex].y1 - dataset[minValueIndex].y1;
				// Use linear interpolation
				y = dataset[minValueIndex].y1 + intervalYDiff * lerp;
			}

			return y;
		}else{
			console.log("Warning: dataset.length <= 1");
			return 0;
		}
	}

	n3Charts.Factory.Tooltip.animoInterpolationFunction = getYForX;

	var createAxes = function(chartDimensions)
	{
        var svg = d3.select("svg.chart");
        svg.append("g")
            .attr("class", "graph-axes-container");

        var xScale = 1;
        var yScale = 1;

        // The x & y axes.
        var xAxis = d3.svg.axis().orient("bottom").scale(xScale).ticks(12, d3.format(",d")),
        yAxis = d3.svg.axis().scale(yScale).orient("left");

        // Add an x-axis label.
        svg.append("text")
            .attr("class", "x-label")
            //.attr("text-anchor", "end")
            .text("Time (minutes) \u2192");

        // Add a y-axis label.
        svg.append("text")
            .attr("class", "y-label")
            //.attr("text-anchor", "end")
            .attr("dy", ".75em")
            .attr("transform", "rotate(-90)")
            .text("Activity (%) \u2192");

		updateAxes(chartDimensions);
	}


	var updateAxes = function(chartDimensions)
	{
		//get svg container
		var margin = {top: 19.5, right: 19.5, bottom: 19.5, left: 39.5};
		if($(".graph-axes-container").length == 0)
		{
			createAxes(chartDimensions);
		}else{
			d3.select(".x-label")
				.attr("x", chartDimensions.width*(2/3))
				.attr("y", chartDimensions.height + 30 );

			d3.select(".y-label")
				.attr("x", -chartDimensions.height/2)
				.attr("y", 2);
		}
	}

	/**
	  add Scrollbar overlay
	 **/
	var addScrollbar = function()
	{
		var chart = d3.select('.chart');
		var container = d3.select('.container');
		var scrollContainer = container.append('g').attr('class','scroll-line-container');
		var line = scrollContainer.append('line');

		scrollContainer.style({
			"cursor":"w-resize"
		});

		line.attr('class', 'scroll-line');
		line.attr({
			'y1': 0,
			'y2': $scope.chartDimensions.height,
			'stroke-width': 4,
			'stroke': '#F01111' //Cosmic Red
		});

		$scope.$watch('vm.scrollBarX', function(vNew, vOld){
			if( !isNaN(vm.scrollBarX))
			{
				vm.selectedTime = vm.scrollBarX/$scope.chartDimensions.width * vm.selectedSimulationEndTime;
				line.attr({
					'x1': vm.scrollBarX,
					'x2': vm.scrollBarX
				});

			}
		});

		$scope.$watch('chartDimensions', function(vNew) {
			line.attr( {'y2' : vNew.height});
			vm.scrollBarX = 0;
		});
		$scope.$watch('vm.selectedSimulationEndTime', function(vNew, vOld){
			$scope.options.axes.x.max = vNew;
			vm.scrollBarX = vm.selectedTime / vm.selectedSimulationEndTime * $scope.chartDimensions.width;
		});

		$scope.$watch('vm.simulationEndTime', function(vNew, vOld){
			vm.selectedSimulationEndTime = vNew;
		});

		var moveBar = function(x)
		{
			x = Math.max($scope.chartDimensions.x1, Math.min($scope.chartDimensions.x2, x)) - $scope.chartDimensions.x1;
			vm.scrollBarX = x;
			$scope.$apply();

			//change network colors
			var ids = Object.keys($scope.data);
			var nodes = cy.nodes();
			cy.startBatch();
			ids.forEach(function(id){

				var id_substr = id.substr(1); //get ID instead of name (?)
				var node = cy.getElementById(id_substr);
				var yValue = getYForX($scope.data[id], vm.selectedTime);
				
				
				//var totalActivity = node.data('levels');
				var totalActivity = 100; //Apparently server normalizes to 100
				var activityRatio = yValue / totalActivity;

				node.data('activityRatio', activityRatio);

				var outgoingEdges = node.outgoers();
				outgoingEdges.forEach(function(edge){
					edge.data('sourceActivityRatio', activityRatio);
				});

			});
			cy.endBatch();
		}

		var drag = d3.behavior.drag()
			.origin(Object)
			.on("drag", function(){
				moveBar(d3.event.sourceEvent.clientX);
			});
		chart.call(drag);

		/**
		  d3.select('.chart').on('click', function()
		  {
		  var x = d3.event.clientX;
		  moveBar(x);
		  });
		 **/

		return line;
	};
});
