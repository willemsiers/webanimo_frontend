// This is the session xml copied from A_B_kinetics.cys.
// It has the following variables you have to replace with integers:
// 
// WEBANIMO_VIEWPORT_WIDTH - The width of the graph window in cytoscape
// WEBANIMO_VIEWPORT_HEIGHT - The height of the graph window in cytoscape
// 
// Currently the values for these are copied from the property "offsetWidth/Height" of the webANIMOEditor element.

var cysession_xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<cysession xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" documentVersion=\"0.9\" id=\"CytoscapeSession-2013_03_18-13_09\">\n    <sessionNote>You can add note for this session here.</sessionNote>\n    <sessionState>\n        <desktop>\n            <desktopSize height=\"1056\" width=\"1868\"/>\n            <networkFrames>\n                <networkFrame y=\"0\" x=\"0\" height=\"WEBANIMO_VIEWPORT_HEIGHT\" width=\"WEBANIMO_VIEWPORT_WIDTH\" frameID=\"Network 0\"/>\n            </networkFrames>\n        </desktop>\n        <server>\n            <ontologyServer/>\n        </server>\n        <cytopanels>\n            <cytopanel id=\"CytoPanel1\">\n                <panelState>dock</panelState>\n                <selectedPanel>4</selectedPanel>\n                <panels>\n                    <panel id=\"test\"/>\n                </panels>\n            </cytopanel>\n            <cytopanel id=\"CytoPanel2\">\n                <panelState>dock</panelState>\n                <selectedPanel>0</selectedPanel>\n                <panels>\n                    <panel id=\"test\"/>\n                </panels>\n            </cytopanel>\n            <cytopanel id=\"CytoPanel3\">\n                <panelState>dock</panelState>\n                <selectedPanel>-1</selectedPanel>\n                <panels>\n                    <panel id=\"test\"/>\n                </panels>\n            </cytopanel>\n        </cytopanels>\n    </sessionState>\n    <networkTree>\n        <network visualStyle=\"ANIMO_3\" viewAvailable=\"true\" id=\"Network 0\" filename=\"Network 0.xgmml\">\n            <parent id=\"Network Root\"/>\n        </network>\n        <network visualStyle=\"default\" viewAvailable=\"false\" id=\"Network Root\" filename=\"Network Root.xgmml\">\n            <parent id=\"NULL\"/>\n            <child id=\"Network 0\"/>\n        </network>\n    </networkTree>\n</cysession>"

// These are the first few lines of the A_B_kinetics.cys network file.
// It has the following variables you have to replace with integers/floats:
//
// WEBANIMO_GRAPH_ZOOM - Zoom of the graph window
// WEBANIMO_GRAPH_CENTER_X - Center x of the graph window
// WEBANIMO_GRAPH_CENTER_Y - Center y of the graph window
//
// Currently the center is calculated by averaging the positions of all the nodes.
// The zoom is calculated by taking the biggest difference between x or y values of the nodes
// and dividing the screen width or height by that difference.

var network_header = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<graph label=\"Network 0\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" xmlns:cy=\"http://www.cytoscape.org\" xmlns=\"http://www.cs.rpi.edu/XGMML\"  directed=\"1\">\n  <att name=\"documentVersion\" value=\"1.1\"/>\n  <att name=\"networkMetadata\">\n    <rdf:RDF>\n      <rdf:Description rdf:about=\"http://www.cytoscape.org/\">\n        <dc:type>Protein-Protein Interaction</dc:type>\n        <dc:description>N/A</dc:description>\n        <dc:identifier>N/A</dc:identifier>\n        <dc:date>2013-03-18 13:09:47</dc:date>\n        <dc:title>Network 0</dc:title>\n        <dc:source>http://www.cytoscape.org/</dc:source>\n        <dc:format>Cytoscape-XGMML</dc:format>\n      </rdf:Description>\n    </rdf:RDF>\n  </att>\n  <att type=\"string\" name=\"backgroundColor\" value=\"#ffffff\"/>\n  <att type=\"real\" name=\"GRAPH_VIEW_ZOOM\" value=\"WEBANIMO_GRAPH_ZOOM\"/>\n  <att type=\"real\" name=\"GRAPH_VIEW_CENTER_X\" value=\"WEBANIMO_GRAPH_CENTER_X\"/>\n  <att type=\"real\" name=\"GRAPH_VIEW_CENTER_Y\" value=\"WEBANIMO_GRAPH_CENTER_Y\"/>\n"

// Note: We turn the cyjs into an other version of the current .cys format. Currently cytoscape 3
// seems to support it and doesn't complain, but it wouldn't surprise me if this breaks in 3 years.

// Turns a graph into an XGMML file with the name "Network 0"
// and returns the XGMML as a string, as well as the biggest difference in
// X cooridinates and difference in Y coordinates.
var makeXGMML = function(graph) {
    // Ensure we get a json obj
    if (typeof graph === "string") {
        graph = JSON.parse(graph).model;
    }

	// Start with the pre-saved header, and continue from there.
    var result = network_header;
	// To keep track of indentation somewhat.
	var depth = 1;

	// Adds identation to result according to depth
	var indent = function() {
		for (var i = 0; i < depth; i++) {
			result += "  ";
		}
	}

	// Inserts an attribute
	// See the existing A_B_kinetics.cys for possible values of type, name, and value.
    var att = function(type, name, value) {
		indent();
        if (typeof value !== "undefined") {
            result += "<att type=\"" + type + "\" name=\"" + name + "\" value=\"" + value + "\"/>\n";
        } else {
			console.log("[Warning] Attribute \"" + name + "\" not found.");
		}
    }

	// Inserts an open node tag and indents
	// See the existing A_B_kinetics.cys for possible values of type, name, and value
    var openNode = function(label, id) {
		indent();
		depth++;
        result += "<node label=\"" + label + "\" id=\"" + id + "\">\n";
    }
    
	// Inserts a closing tag and dedents
    var closeNode = function() {
		depth--;
		indent();
        result += "</node>\n";
    }

	// Inserts an opening edge tag. Indents. Again, check out A_B_kinetics.cys
    var openEdge = function(label, source, target) {
		indent();
		depth++;
        result += "<edge label=\"" + label + "\" source=\"" + source + "\" target=\"" + target + "\">";
    }
	
	// Inserts a closing edge tag and dedents.
    var closeEdge = function() {
		depth--;
		indent();
        result += "</edge>\n";
    }
    
	// Add graph attributes attributes 
    att("integer", "levels", graph.data.levels);
    att("real", "seconds per point", graph.data.seconds_per_point);
    att("real", "time scale factor", graph.data.time_scale_factor);

    var nodes = graph.elements.nodes;
    var edges = graph.elements.edges;

	// Node statistics
	var totalX = 0;
	var totalY = 0;
	var totalNodes = 0;
	var smallestX = 0;
	var smallestY = 0;
	var biggestX = 0;
	var biggestY = 0;

    nodes.forEach(function(node) {
        openNode(node.data.shared_name, node.data.id);

		// Taken from A_B_kinetics.cys
        att("string", "NODE_TYPE", node.data.NODE_TYPE);
        att("real", "Position.X", node.data.Position_X);
        att("real", "Position.Y", node.data.Position_Y);
        att("real", "activityRatio", node.data.activityRatio);
        att("string", "canonicalName", node.data.canonicalName);
        att("boolean", "enabled", node.data.enabled);
        att("integer", "initialConcentration", node.data.initialConcentration);
        att("integer", "levels", node.data.levels);
        att("real", "levels scale factor", node.data.levels_scale_factor); // TODO: This is not present in the cyjs obj!
        att("string", "moleculeType", node.data.moleculeType);
        att("boolean", "plotted", node.data.plotted);

        if (node.data.description != undefined) {
            att("string", "description", node.data.description.replace(/"/g, ""));
        }

        // Add special graphics attribute
		indent();
        result += "<graphics type=\"ELLIPSE\" h=\"55.0\" w=\"55.0\" x=\"" +
            node.position.x +
            "\" y=\"" +
            node.position.y +
            "\" fill=\"#00cc00\" width=\"6\" outline=\"#404040\"/>\n";

        closeNode();

		// This code facilitates the total position for averaging and finds the minimum and maximum coordinates
		totalNodes++;
		totalX += node.position.x;
		totalY += node.position.y;

		smallestX = Math.min(smallestX, node.position.x);
		smallestY = Math.min(smallestY, node.position.y);
		biggestX = Math.max(biggestX, node.position.x);
		biggestY = Math.max(biggestY, node.position.y);
    });

    edges.forEach(function(edge) {
		// Also copied from A_B_kinetics.cys
        openEdge(edge.data.shared_name, edge.data.source, edge.data.target);
        att("string", "canonicalName", edge.data.canonicalName);
        att("boolean", "enabled", edge.data.enabled);
        att("integer", "increment", edge.data.increment);

		indent();
        result += "<att type=\"string\" name=\"interaction\" value=\"" +
            edge.data.interaction +
            "\" cy:editable=\"false\"/>\n";

        att("real", "k", edge.data.k);
        att("string", "output reactant", edge.data.output_reactant);
        att("integer", "scenario", edge.data.scenario);

		indent();
        result += "<graphics width=\"4\" fill=\"#000000\"/>\n";

        closeEdge();
    })

    result += "</graph>";

	// Calculate the final results and already fill in the graph center x and y
	var centerX = totalX / totalNodes;
	var centerY = totalY / totalNodes;

	result = result.replace("WEBANIMO_GRAPH_CENTER_X", centerX);
	result = result.replace("WEBANIMO_GRAPH_CENTER_Y", centerY);

	var diffX = biggestX - smallestX;
	var diffY = biggestY - smallestY;
	
    return {
		xml: result,
		diffX: diffX,
		diffY: diffY
	};
}

var convertToCys = function(graph, name) {
    var zip = new JSZip();

	// Create a session XML with the proper name and with the width and height from the webANIMOEditor element
	var sessionxml = cysession_xml.replace(/Network 0/g, name);
	var webANIMOEditor = $("#webANIMOEditor")[0];
	var viewportHeight = webANIMOEditor.offsetHeight;
	var viewportWidth = webANIMOEditor.offsetWidth;
	sessionxml = sessionxml.replace("WEBANIMO_VIEWPORT_WIDTH", viewportWidth);
	sessionxml = sessionxml.replace("WEBANIMO_VIEWPORT_HEIGHT", viewportHeight);

	// Create a graph xml with the proper name and zoom.
	var graphResult = makeXGMML(graph);
	var graphxml = graphResult.xml.replace(/Network 0/g, name);
	if (graphResult.diffX > graphResult.diffY) {
		graphxml = graphxml.replace("WEBANIMO_GRAPH_ZOOM", viewportWidth / graphResult.diffX);
	} else {
		graphxml = graphxml.replace("WEBANIMO_GRAPH_ZOOM", viewportHeight / graphResult.diffY);
	}

	// Zooming and centering is not perfect, but as long as the graphs are not too "weird (nodes far apart) it's fine

	// Create the zip and save it to disk
    var session = zip.folder("session");
    session.file("cysession.xml", sessionxml);
    session.file("Network 0.xgmml", graphxml);

    var blob = zip.generate({type:"blob"});
    saveAs(blob, name + ".cys");
    return blob;
}
