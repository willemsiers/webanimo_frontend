
var analyse = function() {
	var exportElement = $('#export');
	var oldNetwork = exportElement.val();
	var newNetwork = exportJSON();
	$('#popAnalyse').hide();
	
	//todo: also check for simulation-duration
	if(oldNetwork === newNetwork){
		console.log("Network was unchanged, not uploading");
		displayConsoleMessage("Network was unchanged")
		hidePagePopup();
	}else{
		exportElement.val(newNetwork);
		angular.element(exportElement).triggerHandler('change');
		//#popLoading will be shown in openFile()
	}

};

// http://stackoverflow.com/questions/1043957/clearing-input-type-file-using-jquery
// To make sure the user can load the same file twice
function resetFormElement(e) {
  e.wrap('<form>').closest('form').get(0).reset();
  e.unwrap();

  // Prevent form submission
  e.stopPropagation();
  e.preventDefault();
}

var handleFileSelect = function(evt) {
	var file = evt.target.files[0]; // FileList object

	var textReader = new FileReader();
	textReader.onload = function(evt) {
		saveCurrentState();
		var jsonString = evt.target.result;
		var json = JSON.parse(jsonString);
		loadJson(json);

		$('#mainMenu > li > ul').hide(); 

        displayConsoleMessage("Import of " + file.name + " succesful", "log-success");

        // TODO: Do we need to check for levels_scale_factor as well?
        if (jsonString.search("seconds_per_point") > -1) {
            displayConsoleMessage("Warning: imported model contains an outdated \"seconds_per_point\" attribute. The webANIMO analysis will look squashed.", "log-error", 20000);
        }

		resetFormElement($("#importNetwork"));
	}

    var zipReader = new FileReader();
    zipReader.onload = function(evt) {
        saveCurrentState();

        var result = readCys(evt.target.result);

        if (Array.isArray(result)) {
			displayConsoleMessage("Please select a specific network from the file to import");

			var root = $("#popImport ul");
			root.empty();

			var targets = result;
			var branch = null;
			targets.forEach(function(target, i) {
				if (target.setLabel != branch) {
					root.append($('<li class="header">' + target.setLabel + '</li>'));
					branch = target.setLabel;
				}

				var link = $('<input type="radio" name="importChoice" value="' + i + '"/>');

				if (i == 0) link.attr("checked", "checked");

				var li = $('<li></li>');
				li.append(link);
				li.append(target.graphLabel);
				root.append(li);
			});

			$("#doImport").unbind().click(function() {
				var radios = document.getElementsByName('importChoice');

				for (var i = 0, length = radios.length; i < length; i++) {
					if (radios[i].checked) {
						var graphObj = readCys(evt.target.result, targets[i]);

						if (graphObj == null) {
							return;
						}

						loadJson(graphObj);

						displayConsoleMessage("Import of network " + targets[i].graphLabel + " succesful", "log-success");

						break;
					}
				}

				$('#popPage').hide();
				$('#popPageContent').hide();
				$('#popImport').hide();

				resetFormElement($("#importNetwork"));
			});
			
			$("#doCancelImport").unbind().click(function() {
				$('#popPage').hide();
				$('#popPageContent').hide();
				$('#popImport').hide();

				resetFormElement($("#importNetwork"));
			});

			$('#popPage').show();
			$('#popPageContent').show();
			$('#popImport').show();
        } else if (result == null) {
            // Error!
            // Probably a broken cys file
			displayConsoleMessage("Unable to import the file \"" + file.name + "\"", "log-error", 10000);
        } else {
            // Probably a cyjs object
			loadJson(result);
			displayConsoleMessage("Import of " + file.name + " succesful", "log-success");
        }
    } ;

    if (file.name.search(".cyjs") > -1) {
		console.log('read as text');
    	textReader.readAsText(file);
    } else if (file.name.search(".cys") > -1) {
        console.log('read as cytoscape file');
        zipReader.readAsArrayBuffer(file);
    } else {
        console.log("Unknown filetype");
        displayConsoleMessage("Error: unknown filetype " + file.name, "log-error", 10000);
    }
};

var getShareableLink = function()
{
	var save_script_url = './savenetwork.php';
	var network_json = exportJSON();
	alert('Pressing "Ok" saves the current network in the cloud, and reloads the page with the saved network. The URL in the address bar can be shared with others.');
	$('#popPage').show();
	$('#popPageContent').show();
	$('#popLoading').show();
	$.post(save_script_url, {'network':network_json}).success(function( received_data){
		console.log('success', received_data);
		displayConsoleMessage("Saved network!", 'log-success');
		received_data = JSON.parse(received_data);
		var filename = received_data.file;
		var link = './?network='+filename;
		window.location.replace( link );
	}).
	fail(function(e){
		hidePagePopup();
		var decoded = JSON.parse(e.responseText);
		displayConsoleMessage("Request failed: " + decoded.message, 'log-error', 10000);
	});
}

var exportFunction = function() {
	var date = (new Date()).toISOString().match(/(\d{4}\-\d{2}\-\d{2})T(\d{2}:\d{2}:\d{2})/);
    console.log("Converting graph...");
    convertToCys(exportJSON(), "webANIMO-" + date[1] + ' ' + date[2]);
    console.log("Converting finished");
};

var exportGraphImage = function() {
	var png64 = cy.png({
		full: true,
		scale: 4
	});
	
	var base64 = png64.substr( png64.indexOf(',')+1, png64.length);
	var blob = b64toBlob(base64, 'image/png');
	saveAs(blob, "ANIMO_network.png");
};
