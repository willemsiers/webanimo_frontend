/** CTRL - Z */
var ctrlZ = [];
var ctrlZi = -1;
var lastIndex = -1;
var saveCurrentState = function() {
	if(!showDiff)
	{
		var exported = exportJSON();
		if (lastIndex == ctrlZi && exported == ctrlZ[lastIndex]){
			return;
		}

		lastIndex = ++ctrlZi
			ctrlZ[ctrlZi] = exported;
	}
}
var restorePreviousState = function() {
	if(!showDiff)
	{
		if (ctrlZi == -1) {
			console.log('Empty change history');
			return;
		}

		if (ctrlZi == lastIndex) {
			saveCurrentState();
		}

		cy.load();
		cy.add(JSON.parse(ctrlZ[--ctrlZi]).model.elements);
	}
}
var restoreNextState = function() {
	if(!showDiff)
	{
		if (ctrlZi == lastIndex) {
			console.log('No future history available');
			return;
		}

		cy.load();
		cy.add(JSON.parse(ctrlZ[++ctrlZi]).model.elements);	
	}
}

/** ZOOM */
var	scaleFactor = 0.15;
var zoomOut = function() { 
	cy.zoom(cy.zoom()*(1+scaleFactor));
}
var zoomIn = function() { 
	cy.zoom(cy.zoom()/(1+scaleFactor));
}
var zoomFit = function() {
	cy.fit();
}
var moveRight = function() { 
	cy.panBy({
		'x': -cy.width()*scaleFactor, 
		'y': 0
	});
}
var moveLeft = function() { 
	cy.panBy({
		'x': cy.width()*scaleFactor, 
		'y': 0
	});
}
var moveUp = function() { 
	cy.panBy({
		'x': 0, 
		'y': cy.height()*scaleFactor
	});
}
var moveDown = function() { 
	cy.panBy({
		'x': 0, 
		'y': -1 * cy.height()*scaleFactor
	});
}

/** Screen manipulation */
var handleEscapeDown = function() {
	
	if (inDrag != null) {
		$('#arrow').hide();
		inDrag = null;
	} else
	if ($('#editEdge').is(':visible')) {
		// close edge pop-up
		editEdgeCancel();
	} else 
	if ($('#editNode').is(':visible')) {
		// close node pop-up
		editNodeCancel(); 
	}

};

var handleEnterPress = function(e) {

	if ($('#editEdge').is(':visible')) {
		// finish edge
        if ($('#addEdgeSave').is(':visible'))  addEdgeSave();
		if ($('#editEdgeSave').is(':visible')) editEdgeSave();
	} else 
	if ($('#editNode').is(':visible')) {
		// finish node
		if ($('#addNodeSave').is(':visible'))  addNodeSave();
		if ($('#editNodeSave').is(':visible')) editNodeSave();
	}

	e.preventDefault();
	return false;
}

var handleDelPress = function(e) {
		
	if ($('#editEdge').is(':visible')) {
		// finish edge
		if ($('#addEdgeSave').is(':visible'))  editEdgeCancel();
		if ($('#editEdgeSave').is(':visible')) editEdgeRemove();
	} else 
	if ($('#editNode').is(':visible')) {
		// finish node
		if ($('#addNodeSave').is(':visible'))  editNodeCancel();
		if ($('#editNodeSave').is(':visible')) editNodeRemove();
	}

	e.preventDefault();
	return false;
} 
