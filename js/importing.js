//////////////////////////////
// Main import funcionality //
//////////////////////////////

var readCys = function(input, preferred) {
    var indexOf = function(array, predicate) {
        for (var i = 0, n = array.length; i != n; ++i) {
            if (predicate(array[i])) {
                return i;
            }
        }
        return -1;
    }

    var cysFile = new JSZip(input);
    var files = [];

    for (var filename in cysFile.files) {
        files.push(filename);
    }

    cysFile = {zip: cysFile, files: files};

    var isVersion3 = indexOf(files, function(item) {
        return item.search("3.0.0.version") > -1;
    }) > -1;

    if (typeof preferred === "undefined") preferred = "WEBANIMO_NOT_SET";

    if (isVersion3) {
        return readCys3(cysFile, preferred);
    } else {
        return readCysOld(cysFile, preferred);
    }

    return null;
}

//////////////////////////////////////
// Cytoscape 3 import functionality //
//////////////////////////////////////

var processTable = function(text) {
    var csv = Papa.parse(text);
    var rows = csv.data;

    var result = {};

    // Find column-title to column mapping
    var headerRow = rows[1];
    var colMap = {};
    for (var i = 0; i < headerRow.length; i++) {
        colMap[headerRow[i].replace(/ /g, "_").replace(/\./g, "_")] = i;
    }

    result.rows = rows.slice(5);

    result.getValue = function(row, key) {
        return result.rows[row][colMap[key]];
    }

    result.getCols = function() {
        return Object.keys(colMap);
    }

    return result;
}

var readCys3 = function(cysFile, preferred) {
    var possibleNetworks = cysFile.files.filter(function(item) {
        return item.search("networks") > -1 &&
            item.search(".xgmml") > -1;
    });

    console.log("Reading v3");

    var graphSets = {};
    var all = {
        nodes: {},
        edges: {},
		networks: {}
    }

	for (var i = 0; i < possibleNetworks.length; i++) {
		var xgmml = cysFile.zip.file(possibleNetworks[i]).asText();
		var parser = new DOMParser();
		var dom = parser.parseFromString(xgmml, "application/xml");

		var graphNodes = dom.getElementsByTagName("graph");

        var group = {};
		for (var j = 1; j < graphNodes.length; j++) {
            var graphNode = graphNodes[j];

            var nodes = [];
            var edges = [];

            for (var k = 0; k < graphNode.children.length; k++) {
                var child = graphNode.children[k];
                if (child.tagName == "node") {
                    var nodeId = child.getAttribute("id");
                    nodes.push(nodeId);
                    all.nodes[nodeId] = {
                        label: child.getAttribute("label")
                    }
                } else if (child.tagName == "edge") {
                    var edgeId = child.getAttribute("id");
                    edges.push(edgeId);
                    all.edges[edgeId] = {
                        label: child.getAttribute("label"),
                        source: child.getAttribute("source"),
                        target: child.getAttribute("target")
                    };
                }
            }

            var id = graphNode.getAttribute("id");
            var label = graphNode.getAttribute("label");
            var graphName =  id + "-" + label;
            group[graphName] = {
                id: id,
                label: label,
                nodes: nodes,
                edges: edges,
            };

			all.networks[id] = {
				label: label
			}
		}

		var id = graphNodes[0].getAttribute("id");
		var label = graphNodes[0].getAttribute("label"); 
		all.networks[id] = {
			label: label
		}

		var groupName = id  + "-" + label.replace(" ", "+");
		group.id = id;
		group.label = label;
        graphSets[groupName] = group;
	}

    for (var graphSet in graphSets) {
		var extractInfo = function(name) {
			name = RegExp.escape(name);

			var edgeTables = cysFile.files.filter(function(item) {
				return item.search(name) > -1
					&& item.search("edge") > -1
					&& item.search(".cytable") > -1;
			});
			var nodeTables = cysFile.files.filter(function(item) {
				return item.search(name) > -1
					&& item.search("node") > -1
					&& item.search(".cytable") > -1;
			});
			var networkTables = cysFile.files.filter(function(item) {
				return item.search(name) > -1
					&& item.search("network") > -1
					&& item.search(".cytable") > -1;
			});

			var extractData = function(tableFiles, aggregate) {
				tableFiles.forEach(function(tableFile) {
					var table = processTable(cysFile.zip.file(tableFile).asText());
					table.rows.forEach(function(row, i) {
						var SUID = table.getValue(i, "SUID");
						// If the row has no proper SUID, skip it
						if (isNaN(parseInt(SUID, 10))) {
							return;
						}

						// If SUID not in the aggregate of ALL DATA, ignore it
						if (!(SUID in aggregate)) {
							return;
						}

						table.getCols().forEach(function(column) {
							if (column == "SUID") return;
							aggregate[SUID][column] = table.getValue(i, column);
						});
					});
				});
			};

			extractData(edgeTables, all.edges);
			extractData(nodeTables, all.nodes);
			extractData(networkTables, all.networks);
		}

		extractInfo(graphSet)

		for (var graph in graphSets[graphSet]) {
			extractInfo(graph);
		}
    }

	var nameToNumId = {};
	for (var id in all.nodes) {
		nameToNumId[all.nodes[id].name] = parseInt(id, 10);
	}

	var targets = [];

	console.log(JSON.stringify(graphSets, null, 2));

	for (var graphSet in graphSets) {
		for (var graph in graphSets[graphSet]) {
			if (graph == "id" || graph == "label") {continue;}

			targets.push({
				graphSet: graphSet,
				graph: graph,
				setLabel: graphSets[graphSet].label,
				graphLabel: graphSets[graphSet][graph].label
			});
		}
	}

    if (targets.length == 0) return null;
	else if (targets.length > 1 && preferred == "WEBANIMO_NOT_SET") return targets;
	
	var target = null;
	if (targets.length > 1) {
		target = preferred;
	} else {
		target = targets[0];
	}

	var graphInfo = graphSets[target.graphSet][target.graph];

	var result = {
		"format_version": "1.0",
		"generated_by": "bobescape-3.1.4",
		"target_cytoscapejs_version": "~2.1",
		"data": {
			"SUID": parseInt(graphInfo.id, 10)
		},
		"elements": {
			"nodes": [],
			"edges": []
		}
	}

	var naiveStringCoercion = function(o) {
		if (o == "true") {
			return true;
		} else if (o == "false") {
			return false;
		} 
		
		var parsed = parseFloat(o, 10);

		if (isNaN(parsed)) {
			return o;
		}

		return parsed
	}

	// Get network info
	var networkInfo = all.networks[graphInfo.id];
	for (var key in networkInfo) {
		result.data[key] = naiveStringCoercion(networkInfo[key]);
	}

	graphInfo.nodes.forEach(function(id) {
		var nodeData = {
			id: id
		};
		
		var nodeInfo = all.nodes[id];
		
		for (var key in nodeInfo) {
			nodeData[key] = naiveStringCoercion(nodeInfo[key]);
		}

		// Fuck reality
		if (!('randomInitialConcentration' in nodeData)) {
			nodeData['randomInitialConcentration'] = false;
		}
		if (typeof(nodeData['randomInitialConcentration']) != "boolean") {
			nodeData['randomInitialConcentration'] = false;
		}

		result.elements.nodes.push({data: nodeData});
	});

	graphInfo.edges.forEach(function(id) {
		var edgeData = {
			id: id
		};

		var edgeInfo = all.edges[id];

		for (var key in edgeInfo) {
			edgeData[key] = naiveStringCoercion(edgeInfo[key]);
		}

		// Compensate for the imperfections of our puny reality
		if (edgeData._REACTANT_ACT_E1 == "") {
			edgeData._REACTANT_ACT_E1 = false;
			edgeData._REACTANT_ACT_E2 = false;
			delete edgeData._REACTANT_E1;
			delete edgeData._REACTANT_E2;
		}

		// Compensate some more
		edgeData.output_reactant = nameToNumId[edgeData.output_reactant];
		edgeData.source = edgeData.source + '';
		edgeData.target = edgeData.target + '';

		result.elements.edges.push({data: edgeData});
	});

	return result;
}

// http://stackoverflow.com/questions/3561493/is-there-a-regexp-escape-function-in-javascript/3561711#3561711
RegExp.escape = function(s) {
    return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
};

///////////////////////////////////////
// Cytoscape Old import funcionality //
///////////////////////////////////////

var readCysOld = function(cysFile, preferred) {
	console.log("Reading old");

	var possibleNetworks = cysFile.files.filter(function(filepath) {
		return filepath.search(".xgmml") > -1;
	});
	var parser = new DOMParser();

	if (preferred == "WEBANIMO_NOT_SET") {
		// Return list of possible networks
		var networkNames = [];

		possibleNetworks.forEach(function(filepath) {
			var xgmml = cysFile.zip.file(filepath).asText();
            var dom = parser.parseFromString(xgmml, "application/xml");

			var graphNode = dom.getElementsByTagName("graph")[0];
			var graphLabel = graphNode.getAttribute("label");
			networkNames.push({
				graphSet: graphLabel,
				graph: graphLabel,
				setLabel: graphLabel,
				graphLabel: graphLabel
			});
		});

		if (networkNames.length == 1) {
			preferred = networkNames[0];
		} else {
			return networkNames;
		}
	}

	// Find network with name preferred
	var dom = null;
	possibleNetworks.forEach(function(filepath) {
		var xgmml = cysFile.zip.file(filepath).asText();
		var specificDom = parser.parseFromString(xgmml, "application/xml");

		var graphNode = specificDom.getElementsByTagName("graph")[0];
		var graphLabel = graphNode.getAttribute("label");
		if (graphLabel == preferred.graph) {
			dom = specificDom;
		}
	});

	if (dom == null) {
		return null;
	}
	
	// Construct network
	var result = {
		data: {
			name: preferred.graph,
			shared_name: preferred.graph
		},
		elements: {
			nodes: [],
			edges: []
		}
	};

	var graphNode = dom.getElementsByTagName("graph")[0];

	for (var i = 0; i < graphNode.children.length; i++) {
		var e = graphNode.children[i];

		if (e.tagName != "att") continue;

		var name = e.getAttribute("name");
		var value = e.getAttribute("value");
		if (name == "levels") {
			result.data.levels = parseInt(value, 10);
		};
	}

	var labelToIdMap = {};

	var oldToNewIdMap = {
		nextId: 1
	};
	oldToNewIdMap.next = function() {
		oldToNewIdMap.nextId += 1;
		return oldToNewIdMap.nextId;
	}


	readCysOldNodeInfo(graphNode, labelToIdMap, oldToNewIdMap, result);

	readCysOldEdgeInfo(graphNode, labelToIdMap, oldToNewIdMap, result);

	return result;
}

var readCysOldNodeInfo = function(graphNode, labelToIdMap, oldToNewIdMap, result) {
	var xmlNodes = graphNode.getElementsByTagName("node");
	xmlNodes = Array.prototype.slice.call(xmlNodes);
	var maxId = 0;
	xmlNodes.forEach(function(xmlNode) {
		var node = {
			data: {
				id: oldToNewIdMap.next(),
				randomInitialConcentration: false,
				name: xmlNode.getAttribute("label")
			},
			position: {}
		};

		oldToNewIdMap[xmlNode.getAttribute("id")] = node.data.id;

		if (parseInt(node.data.id, 10) > maxId) {
			maxId = parseInt(node.data.id, 10);
		}

		labelToIdMap[xmlNode.getAttribute("label")] = node.data.id;

		for (var i = 0; i < xmlNode.children.length; i++) {
			var child = xmlNode.children[i];
			if (child.tagName == "att") {
				var name = child.getAttribute("name");
				var value = child.getAttribute("value");

				if (name == "activityRatio") {
					node.data.activityRatio = parseInt(value);
				} else if (name == "plotted") {
					node.data.plotted = value == "true";
				} else if (name == "NODE_TYPE") {
					node.data.NODE_TYPE = value;
				} else if (name == "enabled") {
					node.data.enabled = value == "true";	
				} else if (name == "shared name") {
					node.data.shared_name = value;
				} else if (name == "Position.X") {
					node.data.Position_X = parseFloat(value);
				} else if (name == "Position.Y") {
					node.data.Position_Y = parseFloat(value);
				} else if (name == "moleculeType") {
					node.data.moleculeType = value;
				} else if (name == "name") {
					node.data.name = value;
				} else if (name == "randomInitialConcentration") {
					node.data.randomInitialConcentration = value == "true";
				} else if (name == "initialConcentration") {
					node.data.initialConcentration = parseInt(value, 10);
				} else if (name == "levels") {
					node.data.levels = parseInt(value, 10);
				} else if (name == "canonicalName") {
					node.data.canonicalName = value;
				} else if (name == "description") {
					node.data.description = value;
				} else if (name == "levels scale factor") {
					node.data.levels_scale_factor = parseFloat(value);
				}
			}
		}

		node.data.shared_name = node.data.name;
		node.data.alias = node.data.canonicalName;

		node.position.x = node.data.Position_X;
		node.position.y = node.data.Position_Y;

		result.elements.nodes.push(node);
	});

	return maxId;
}

var readCysOldEdgeInfo = function(graphNode, labelToIdMap, oldToNewIdMap, result) {
	var xmlEdges = graphNode.getElementsByTagName("edge");
	xmlEdges = Array.prototype.slice.call(xmlEdges);
	xmlEdges.forEach(function (xmlEdge) {
		var edge = {
			data: {
				id: oldToNewIdMap.next(),
				source: oldToNewIdMap[xmlEdge.getAttribute("source")],
				target: oldToNewIdMap[xmlEdge.getAttribute("target")],
				shared_name: xmlEdge.getAttribute("label")
			}
		};

		for (var i = 0; i < xmlEdge.children.length; i++) {
			var child = xmlEdge.children[i];
			if (child.tagName == "att") {
				var name = child.getAttribute("name");
				var value = child.getAttribute("value");

				if (name == "canonicalName") {
					edge.data.canonicalName = value;
				} else if (name == "enabled") {
					edge.data.enabled = value == "true";
				} else if (name == "increment") {
					edge.data.increment = parseInt(value, 10);
				} else if (name == "interaction") {
					edge.data.interaction = value;
				} else if (name == "k") {
					edge.data.k = parseFloat(value);
				} else if (name == "output reactant") {
					if (value != "") {
						edge.data.output_reactant = value;
					}
				} else if (name == "scenario") {
					edge.data.scenario = parseInt(value, 10);
				} else if (name == "_REACTANT_ACT_E1") {
					edge.data._REACTANT_ACT_E1 = value == "true";
				} else if (name == "_REACTANT_ACT_E2") {
					edge.data._REACTANT_ACT_E2 = value == "true";
				} else if (name == "_REACTANT_E1") {
					edge.data._REACTANT_E1 = parseInt(labelToIdMap[value], 10);
				} else if (name == "_REACTANT_E2") {
					edge.data._REACTANT_E2 = parseInt(labelToIdMap[value], 10);
				} else if (name == "description") {
					edge.data.description = value;
				}
			}
		}

		edge.data.name = edge.data.canonicalName;

		result.elements.edges.push(edge);
	});
}
