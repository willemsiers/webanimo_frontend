$( document ).ready(function() { // on dom ready

	$('#cy').cytoscape({

		style: cytoscape.stylesheet()
			.selector('node')
			.css({
				'content': 'data(canonicalName)',
				'text-valign': 'center',
				'text-halign': 'center',
				'font-family': 'Montserrat',
				'color': 'black',
				'width': '50',
				'height': '50',
				'background-color': 'grey',
				'border-width': '4',
				'border-color': 'rgb(62,62,62)',
				'text-outline-width': 2,
				'text-outline-color': 'white'
			}) .selector('node[moleculeType = "Cytokine"]')
		.css({
			'content': 'data(canonicalName)',
			'shape': 'rectangle',
		}) .selector('node[moleculeType = "Receptor"]')
		.css({
			'content': 'data(canonicalName)',
			'height': '75',
		}) .selector('node[moleculeType = "Kinase"]')
		.css({
			'content': 'data(canonicalName)',
		}) .selector('node[moleculeType = "Phosphatase"]')
		.css({
			'content': 'data(canonicalName)',
			'shape': 'diamond',
		}) .selector('node[moleculeType = "Transcription factor"]')
		.css({
			'content': 'data(canonicalName)',
			'width': '75',
		}) .selector('node[moleculeType = "Gene"]')
		.css({
			'content': 'data(canonicalName)',
			'shape': 'triangle',
		}) .selector('node[moleculeType = "mRNA"]')
		.css({
			'content': 'data(canonicalName)',
			'shape': 'rhomboid',
		}) .selector('node[moleculeType = "Other"]')
		.css({
			'content': 'data(canonicalName)',
			'shape': 'rectangle',
			'width': '75',
		}) .selector('node[!enabled]')
		.css({
			"background-opacity" : "0.23529411764705882",
		}) .selector('node[activityRatio > 1]')
		.css({
			"background-color" : "rgb(62,213,37)"
		}).selector('node[activityRatio = 1]')
		.css({
			"background-color" : "rgb(62,213,37)"
		}) .selector('node[activityRatio = 0]')
		.css({
			"background-color" : "rgb(236,59,59)"
		}) .selector('node[activityRatio < 0]')
		.css({
			"background-color" : "rgb(236,59,59)"
		}) .selector('node[activityRatio > 0.5][activityRatio < 1]')
		.css({
			"background-color" : "mapData(activityRatio,0.5,1,rgb(255,204,0),rgb(62,213,37))"
		}) .selector('node[activityRatio > 0][activityRatio <= 0.5]')
		.css({
			"background-color" : "mapData(activityRatio,0,0.5,rgb(236,59,59),rgb(255,204,0))"
		})
		.selector('edge')
			.css({
				"font-size" : "10",
				"source-arrow-color" : "rgb(62,62,62)",
				"line-color" : "rgb(62,62,62)",
				"content" : "",
				"target-arrow-color" : "rgb(62,62,62)",
				"source-arrow-shape" : "none",
				"text-opacity" : "1.0",
				"width" : "mapData(sourceActivityRatio,0.0,1.0, 3, 8)",
				"target-arrow-shape" : "none",
				"line-style" : "solid",
			})  .selector('edge[increment = 1]')
		.css({
			"target-arrow-shape" : "triangle",

		}) 	.selector('edge[increment = -1]')
		.css({
			"target-arrow-shape" : "tee",
		})	.selector('edge[!enabled]')
		.css({
			"opacity" : "0.23529411764705882",
		}) .selector('.currentActive')
			.css({
			'border-color': 'rgb(53,38,255)',
			'line-color': 'rgb(53,38,255)',
			'target-arrow-color': 'rgb(53,38,255)'
		}),

		ready: function() {
			window.cy = this;

			cy.elements().unselectify();
		}

	});
	
	// set the layout options
	var options = {
		name: 'preset',
		positions: undefined,
		zoom: undefined, // the zoom level to set (prob want fit = false if set)
		pan: undefined, // the pan level to set (prob want fit = false if set)
		fit: true, // whether to fit to viewport
		padding: 30, // padding on fit
		animate: false, // whether to transition the node positions
		animationDuration: 500, // duration of animation in ms if enabled
		animationEasing: undefined, // easing of animation if enabled
		ready: undefined, // callback on layoutready
		stop: undefined // callback on layoutstop
	};

	cy.layout( options );

	//$('#load').click(function() {
		//cy.load(JSON.parse($('#networkJSON').html()).elements);
	//});
//console.log($('#networkJSON').html());
	//cy.load(JSON.parse($('#networkJSON').html()).elements);
	//console.log($('#networkJSON').html());

}); // on dom ready
