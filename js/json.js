/** ANALYSE DATA OPTION **/
var exportJSON = function() {
	var nodeList = [];
	var nodes = cy.nodes();
	$.each(nodes, function(i, node) {
		var xPos = node.position('x');
		var yPos = node.position('y');
		var nodeData = {
			'data' : node.data(),
			'position': {
				'x': xPos,
				'y': yPos
			}
		};
		nodeData.data.Position_X = xPos;
		nodeData.data.Position_Y = yPos;
		nodeList.push(nodeData);
	});
	var edgeList = [];
	var edges = cy.edges();
	$.each(edges, function(i, edge) {
		var sEdge = {'data' : edge.data()}
		sEdge.data.k *= 1.0; // cast possible int to double
		edgeList.push(sEdge);
	});
	
	var elements = {
		nodes: nodeList,
		edges: edgeList 
	};

	//TODO: !!! Find better way to get revision's models in the log (used for showing diff's from PREVIOUS revisions)
	projectLog[projectLog.length-1].finalGraph = elements;	

	var exportData = { 
		model: { 
			data: {
				shared_name: "Network",
				name: "Network",
				seconds_per_point: 1.0,
				levels: 100
			},
			elements: elements,
			projectLog: $.isEmptyObject(projectLog[projectLog.length-1].changes) ? projectLog.slice(0, projectLog.length-1) : projectLog
		},
		minutesToSimulate: parseInt($('#minutesToSimulate').val())
	};

	return JSON.stringify(exportData);
};

function loadJson(json) {
	var elements = json.elements;
	clearProjectLog();
	projectLog = json.projectLog || [];

	var version_last = ( projectLog[projectLog.length - 1] &&  projectLog[projectLog.length - 1].revision )  || 0;
	projectLog.push(
			{
				'revision': version_last + 1,
				'changes': {}
			});

	saveCurrentState();

    var needsLayout = 0;

	// loads a network and takes care of the positions
	cy.load();
	// nodes
	$.each(elements.nodes, function(i, node) {
		var log = node.data.log;
		if(log || $.isEmptyObject(log))
		{
			delete node.data.log;
		}
		if (node.position == null) {
			if(node.data.Position_X != undefined && node.data.Position_Y != undefined)
			{
				node.position = {
					x: node.data.Position_X,
					y: node.data.Position_Y
				}
			} else {
                node.position = {x: 0, y: 0};
				needsLayout++;
			}
		}
		cy.add(node);
	});
	//edges
	$.each(elements.edges, function(i, edge) {
		var log = edge.data.log;
		if(log || $.isEmptyObject(log))
		{
			delete edge.data.log;
		}
		cy.add(edge);
	});

    if (needsLayout > elements.nodes.length * 0.5) {
        cy.layout({name: 'cose'});
    }

	cy.fit();
	projectLog[projectLog.length-1].finalGraph = elements;	
}
