/** Add an edge */
function addEdge(sourceVal, targetVal, descriptionVal, incrementVal, kVal, scenarioVal, 
					e1active, e2active, e1react, e2react, other, isEnabled) {
	if (typeof(other) == "undefined") other = " ";
	var edgeData = [{ group: "edges", data: { 
		source: sourceVal,
		target: targetVal,
		shared_interaction : "DefaultEdge",
		description: descriptionVal,
		increment: incrementVal,
		k: kVal,
		enabled: isEnabled,
		scenario: scenarioVal,
		interaction : "DefaultEdge",
		selected : false,
		_REACTANT_ACT_E2: e2active,
		_REACTANT_ACT_E1: e1active,
		_REACTANT_E2: e2react,
		_REACTANT_E1: e1react,
		newEdgeID: other
	}}];
	
	var edge = cy.add(edgeData);
	addLogEntry (edge, 'created', 'revision #'+projectLog.length);
	return edge;
}

/** Add a node */
function addNode(initialConcentrationVal, levelsVal, plottedVal, descriptionVal, enabledVal, moleculeTypeVal, canonicalNameVal, posX, posY) {
	var node = cy.add([{ group: "nodes", data: { 
		activityRatio: (initialConcentrationVal / levelsVal),
		plotted: plottedVal, 
		NODE_TYPE: "DefaultNode",
		description: descriptionVal,
		enabled: enabledVal, 
		moleculeType: moleculeTypeVal,
		randomInitialConcentration: false,
		initialConcentration: initialConcentrationVal,
		selected: false,
		name: "",
		levels: levelsVal,
		canonicalName: canonicalNameVal
		}, 
		renderedPosition: { 
		x: posX, y: posY } 
	}]);
	
	if (canonicalNameVal.toLowerCase() == 'lama' || canonicalNameVal.toLowerCase() == 'llama' || canonicalNameVal.toLowerCase() == 'alpaca') {
		$('#alpaca').show();
		$('#llama').attr('src', './images/alpaca.jpg');
	}	
	
	// DOES THIS FIX THE INVISIBLE EXISTING NODE PROBLEM?
	cy.elements("node:visible").select().unselect();
	addLogEntry (node, 'created', 'revision #'+projectLog.length);
	return node;
}

function adjustArrow(e) {
	if (inDrag == null)
		return;

	// going to calculate length and angle
	var posX = e.cyRenderedPosition.x;
	var posY = e.cyRenderedPosition.y;

	var node = cy.getElementById(inDrag);
	// show the arrow
	var nX = node.renderedPosition("x");
	var nY = node.renderedPosition("y");
	
	var dX = nX - posX;
	var dY = nY - posY;
	
	$('#arrow').css('left', nX);
	$('#arrow').css('top', nY);
	
	$('#arrow').css('width', Math.sqrt(dX*dX+dY*dY));
	$('#arrow').css('transform', "rotate(" + Math.atan2(dY * -1, dX * -1) + 'rad)');
}

function closeArrow(e) {
	
	if (inDrag == null)
		return;

	$('#arrow').hide();
	inDrag = null;
}

function deleteNode() {
	// add ctrl+Z
	saveCurrentState();
	var id = $('#nodeId').val();
	var el = cy.getElementById(id);
	var removedElements = cy.remove(el);

	$.each(removedElements, function(name, el)
			{
				var group = typeof(el.data('source')) === 'undefined' ? 'nodes' : 'edges';
				addLogDeleted(el, group);
			});

}

function deleteEdge() {
	// add ctrl+Z
	saveCurrentState();
	var id = $('#edgeId').val();
	var el = cy.getElementById(id);
	var edge = cy.remove(el);
	addLogDeleted(edge, 'edges');
}

/** Clear network  */
var clearNetwork = function(skipConfimation)
{
	saveCurrentState();
	clearProjectLog();
	cy.load();
	saveToLocalStorage();
	displayConsoleMessage("Network cleared", 'log-success');
};

/** Save edge clicked */
var addEdgeSave = function() {
	// add ctrl+Z
	saveCurrentState();

	cy.elements().removeClass('currentActive');
	$('#arrow').hide();
	var error = false;
	$('#source').removeClass('error');
	$('#target').removeClass('error');
	$('#k').removeClass('error');
	
	if ($('#source').val() == null) {
		$('#source').addClass('error');
		error = true;
	} 
	
	if ($('#target').val() == null) {
		$('#target').addClass('error');
		error = true;
	}
	
	if ($('#k').val() == "") {
		$('#k').addClass('error');
		error = true;
	}	
	
	if (!error) {
		hideReactionLabels();
		$('#editEdge').hide();
		$('#popBG').hide();
		$('#popContent').hide();
		$('#leftBarContent').show();
		var incrementValue;
		e1bool = $('#e1active').val() == 'true';
		e2bool = $('#e2active').val() == 'true';
		e1react = $('#e1reactant').val();
		e2react = $('#e2reactant').val();
		$('#activation').prop("checked") ? incrementValue = 1 : incrementValue = -1;
		var edge = addEdge($('#source').val(), $('#target').val(), $('#description').val(), incrementValue, parseFloat($('#k').val()), parseInt($('#reactionType').val()), 
						e1bool, e2bool, e1react, e2react, "", $('#enabledEdge').prop("checked"));
		$('#source').removeClass('error');
		$('#target').removeClass('error');
		$('#k').removeClass('error');	
	}
};

/** Save edited edge */ 
var editEdgeSave = function() {
	// add ctrl+Z
	saveCurrentState();

	// BATCH
	cy.startBatch();
	$('#arrow').hide();
	//cy.elements().removeClass('currentActive');
	//$('#editEdge').hide();
	//$('#popBG').hide();
	//$('#popContent').hide();
	//hideReactionLabels();
	// going to store the data
	var edge = cy.getElementById($('#edgeId').val());
	var incrementValue;
	$('#activation').prop("checked") ? incrementValue = 1 : incrementValue = -1;
	updateDataValue(edge, "increment", incrementValue);
	updateDataValue(edge, "k", parseFloat($('#k').val()));
	updateDataValue(edge, "scenario", parseInt($('#reactionType').val()));
	var e1bool = true;
	var e2bool = true;
	var e1react = 0;
	var e2react = 0;
	if ($('#reactionType').val() == 2) {
		e1bool = $('#e1active').val() == 'true';
		e2bool = $('#e2active').val() == 'true';
		e1react = $('#e1reactant').val();
		e2react = $('#e2reactant').val();
		updateDataValue(edge, "_REACTANT_ACT_E1", e1bool);
		updateDataValue(edge, "_REACTANT_ACT_E2", e2bool);
		updateDataValue(edge, "_REACTANT_E1", e1react);
		updateDataValue(edge, "_REACTANT_E2", e2react);
	}
	updateDataValue(edge, "enabled", $('#enabledEdge').prop("checked") == true);
	updateDataValue(edge, "description", $('#edgeDescription').val());
	// if source or targets are changed we'll delete the edge and create a new one
	if ($('#source').val() != edge.data("source") || $('#target').val() != edge.data("target")) {
		addLogDeleted(edge, 'edges');
		cy.remove(edge);
		var otherFlag = "iAmTheLlama";
		addEdge($('#source').val(), $('#target').val(), $('#edgeDescription').val(), incrementValue, parseFloat($('#k').val()), 
			parseInt($('#reactionType').val()), e1bool, e2bool, e1react, e2react, otherFlag, $('#enabledEdge').prop("checked"));
		$('#editEdge').hide();
		$('#popBG').hide();
		$('#popContent').hide();
		$('#leftBarContent').show();
		
		var newEdge = cy.edges("[newEdgeID='iAmTheLlama']");
		newEdge.data('newEdgeID', '');
		cy.elements().removeClass('currentActive');
		editEdgeShizzle(newEdge);
		newEdge.source().addClass('currentActive');
		newEdge.target().addClass('currentActive');
		newEdge.addClass('currentActive');
	}
	cy.endBatch();
};

/** NODES */
var editNodeSave = function() {
	// add ctrl+Z
	saveCurrentState();

	// BATCH
	cy.startBatch();
	//cy.elements().removeClass('currentActive');
	//$('#editNode').hide();
	//$('#popBG').hide();
	//$('#popContent').hide();
	// going to store the data
	var node = cy.getElementById($('#nodeId').val());
	updateDataValue(node, "moleculeType", $('#moleculeType').val());
	updateDataValue(node, "canonicalName", $('#name').val());
	updateDataValue(node, "initialConcentration", parseInt($('#initialActivity').val()));
	updateDataValue(node, "levels", parseInt($('#totalActivity').val()));
	// set the ratio
	var activityRatio = $('#initialActivity').val() / $('#totalActivity').val();
	updateDataValue(node, "activityRatio", activityRatio);
	// set the radio buttons
	updateDataValue(node, "enabled", $('#enabled').prop("checked"));
	updateDataValue(node, "plotted", $('#plotted').prop("checked"));
	// set the description
	updateDataValue(node, "description", $('#nodeDescription').val());
	// set the neighbour edges enabled to disabled
	var nodes = node.neighborhood('node');
	$.each(nodes, function(index, element) {
		var enabled = (element.data("enabled") && $('#enabled').prop("checked"));
		updateDataValue(node.edgesWith(element), "enabled", enabled);
	});
	cy.endBatch();
};

/** Save add node window */
var addNodeSave = function() {
	// add ctrl+Z
	saveCurrentState();

	cy.elements().removeClass('currentActive');
	$('#editNode').hide();
	$('#popBG').hide();
	$('#popContent').hide();
	$('#leftBarContent').show();
	var node = addNode(parseInt($('#initialActivity').val()), parseInt($('#totalActivity').val()), $('#plotted').prop("checked"), 
		$('#nodeDescription').val(), $('#enabled').prop("checked"), $('#moleculeType').val(), $('#name').val(), $('#posX').val(), $('#posY').val());
};
