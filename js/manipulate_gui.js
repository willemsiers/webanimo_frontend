/** EDGES */
var editEdge = function(e) {
	editEdgeShizzle(e.cyTarget);
}

var editEdgeShizzle = function(e) {
	var edge = e;

	$('input').removeClass('diff-input-changed');
	if(showDiff)
	{
		highlightDiffInputs(selectedRevision, edge);
	}

	hideReactionLabels();
	// Open the correct window 
	$('#editEdge').show();
	$('#editNode').hide();
	$('#popBG').show();
	$('#popContent').show();
	$('#leftBarContent').hide();
	$('#editEdgeLabel').html('Edit interaction');
	$('#addEdgeSave').hide();
	$('#editEdgeSave').show();
	$('#editEdgeRemove').show();
	// get the source and the target
	var source = cy.getElementById(edge.data("source"));
	var target = cy.getElementById(edge.data("target"));
	var arrow = " --> ";
	if (edge.data("increment") == -1) 
		arrow = " --| ";
	$('#edgeLabel').html(source.data("canonicalName") + arrow + target.data("canonicalName"));
	// set the target and source
	var nodes = cy.nodes();
	var selectData = "";
	var labelSelectData = "";
	// create the select options
	$('#edgeId').val(edge.data("id"));
	$.each(nodes, function(index, element) {
		var option = "<option value='" + element.data("id") + "'" + ">" + element.data("canonicalName") + "</option>";
		selectData = selectData + option;
		if (element.data("canonicalName") == source.data("canonicalName")) {
			labelSelectData = labelSelectData + "<option value='" + element.data("id") + "'" + ">" + element.data("canonicalName") + " (the upstream reactant)</option>";
		} else if (element.data("canonicalName") == target.data("canonicalName")) {
			labelSelectData = labelSelectData + "<option value='" + element.data("id") + "'" + ">" + element.data("canonicalName") + " (the downstream reactant)</option>";
		} else {
			labelSelectData = labelSelectData + option;
		}
	});
	$('#source').html(selectData);
	$('#target').html(selectData);
	$('#source').val(source.data("id"));
	$('#target').val(target.data("id"));
	$('#e1reactant').html(labelSelectData);
	$('#e2reactant').html(labelSelectData);
	// set increment
	var increment = edge.data("increment") == 1;
	$('#activation').prop("checked", increment);
	$('#inhibition').prop("checked", !increment);
	$('#enabledEdge').prop("checked", edge.data("enabled"));
	$('#disabledEdge').prop("checked", !edge.data("enabled"));
	// set reaction
	$('#reactionType').val(edge.data("scenario"));
	$('#reactionLabel' + edge.data("scenario")).show();
	$('#reactionLabel0').html('E = active ' + source.data("canonicalName"));
	$('#reactionLabel1').html('E = active ' + source.data("canonicalName") + '<br />S = active ' + target.data("canonicalName"));
	
	var k = edge.data("k");
	$('#k').val(k);
	
	var sliderValue = Math.log2(k * 1000);
	$('#k_slider').val(sliderValue);
	
	// description
	$('#edgeDescription').val(edge.data("description"));
	// try to set reaction 3 values if exists
	if (edge.data("scenario") == 2) {
		$('#e1active').val("" + edge.data("_REACTANT_ACT_E1"));
		$('#e2active').val("" + edge.data("_REACTANT_ACT_E2"));
		$('#e1reactant').val(edge.data("_REACTANT_E1"));
		$('#e2reactant').val(edge.data("_REACTANT_E2"));
	} else {
		$('#e1active').val("true");
		$('#e2active').val("true");
		$('#e1reactant').val(source.data("id"));
		$('#e2reactant').val(target.data("id"));
	}
}
/** Add edge window */
var openEdgeEdit = function() {
	$('#edgeLabel').html('');
	hideReactionLabels();
	$('#reactionLabel0').show();
	$('#editEdge').show();
	$('#popBG').show();
	$('#popContent').show();
	$('#editEdgeLabel').html('Add interaction');
	$('#leftBarContent').hide();
	$('#addEdgeSave').show();
	$('#editEdgeSave').hide();
	$('#editEdgeRemove').hide();
	$('#editNode').hide();
	$('#activation').prop("checked", true);
	$('#reactionType').val(0);
	$('#enabledEdge').prop("checked", true);
	$('#disabledEdge').prop("checked", false);
	$('#k').val(0.001);
	var nodes = cy.nodes();
	var options = "";
	$.each(nodes, function(index, element) {
		options = options + "<option value='" + element.data("id") + "'" + ">" + element.data("canonicalName") + "</option>";
	});
	$('#source').html(options);
	$('#target').html(options);
	$('#e1reactant').html(options);
	$('#e2reactant').html(options);
	var changeEdge = false;
};

/** Cancel edge edit */
var editEdgeCancel = function() {
	$('#arrow').hide();
	cy.elements().removeClass('currentActive');
	$('#editEdge').hide();
	$('#popBG').hide();
	$('#popContent').hide();
	$('#leftBarContent').show();
	hideReactionLabels();
	$('#source').removeClass('error');
	$('#target').removeClass('error');
	$('#k').removeClass('error');
};

var editEdgeRemove = function() {
	$('#editEdge').hide();
	$('#popBG').hide();
	$('#popContent').hide();
	$('#leftBarContent').show();
	hideReactionLabels();
	deleteEdge();
};

var addNodeEdge = function(e) {
	cy.elements().removeClass('currentActive');
	inDrag = $('#nodeId').val();
	var node = cy.getElementById(inDrag);
	// show the arrow
	var posX = node.renderedPosition("x");
	var posY = node.renderedPosition("y");
	$('#arrow').css("left", posX);
	$('#arrow').css("top", posY);
	$('#arrow').css("width", "1px");
	$('#arrow').css('background-color', 'black');
	$('#arrow').show();
};

var updateDataValue = function(node, field_name, new_value)
{
	//'node' may also be an edge
	//update node data only if changed. Also update log if necesary
	var old_value = node.data(field_name);
	if(old_value !== new_value)
	{
		addLogEntry(node, field_name, new_value);
		node.data(field_name, new_value);
	}
}

/** Cancel node edit */
var editNodeCancel = function() {
	cy.elements().removeClass('currentActive');
	$('#popBG').hide();
	$('#popContent').hide();
	$('#editNode').hide();
	$('#leftBarContent').show();
}

/** Delete a node */
var editNodeRemove = function() {

	$('#editNode').hide();
	$('#popBG').hide();
	$('#popContent').hide();
	$('#leftBarContent').show();
	hideReactionLabels();
	
	deleteNode();
};

var addNodeOpen = function() {
	cy.elements().removeClass('currentActive');
	$('#editNode').show();
	$('#popBG').show();
	$('#popContent').show();
	$('#editEdge').hide();
	$('#leftBarContent').hide();
	$('#editNodeLabel').html('Add reactant');
	$('#editNodeRemove').hide();
	$('#editNodeSave').hide();
	$('#addNodeSave').show();
	// set default values
	$('#enabled').prop("checked", true);
	$('#plotted').prop("checked", true);
	$('#totalActivity').val(100);
	$('#levelLabel').html(100);
	$('#initialActivity').prop("max", 100);
	$('#initialLabel').html(0);
	$('#initialActivity').val($('#initialLabel').html());
	$('#moleculeType').val("Cytokine");
	$('#name').val("");
	$('#name').focus();
    $('#nodeDescription').val('');
}

var highlightDiffInputs = function(revision, node)
{
	$('.inputBox').each( function(i, inputbox){
		$(inputbox).find('input,select,textarea,div').each(function(j, input){
			$(input).removeClass('diff-input-changed');
		});
	});

	var changes = getNodeChanges(node, getRevisionChanges(revision));
	var isEdge = typeof(node.data('source')) !== 'undefined';
	if(changes && !($.isEmptyObject(changes)))
	{
		node.addClass('has-changes');
		$.each(changes, function(name, change) {
			var selector = propertyToInputSelector(name, isEdge);
			console.log('changed', name, isEdge, selector);
			$(selector).addClass('diff-input-changed');
		});
	}else
	{
		node.addClass('no-changes');
	}
};

var editNode = function(e) {

	var node = e.cyTarget;

	$('input').removeClass('diff-input-changed');
	if(showDiff)
	{
		highlightDiffInputs(selectedRevision,node);
	}

	// no set source, edit node
	if (inDrag == null) {

		// Open and hide the correct windows 
		$('#editNode').show();
		$('#popBG').show();
		$('#popContent').show();
		$('#editEdge').hide();
		$('#editNodeRemove').show();
		$('#editNodeSave').show();
		$('#addNodeSave').hide();
		$('#leftBarContent').hide();
		$('#editNodeLabel').html('Edit reactant');
		// Set the known values in the input boxes
		$('#name').val(node.data("canonicalName"));
		$('#moleculeType').val(node.data("moleculeType"));
		$('#totalActivity').val(node.data("levels"));
		$('#initialActivity').prop('max', node.data("levels"));
		$('#levelLabel').html(node.data("levels"));
		$('#initialActivity').val(node.data("initialConcentration"));
		$('#initialLabel').html(node.data("initialConcentration"));
		$('#nodeId').val(node.data("id"));
		// set the radio buttons
		var enabled = node.data("enabled");
		$('#enabled').prop("checked", enabled);
		$('#disabled').prop("checked", !enabled);
		var plotted = node.data("plotted"); 
		$('#plotted').prop("checked", plotted);
		$('#hidden').prop("checked", !plotted);
		// set description
		$('#nodeDescription').val(node.data("description"));

		changeNode = false;

	} else { // set source and this is target, create edge
		openEdgeEdit(); 

		$('#source').val(inDrag);
		$('#target').val(node.data('id'));
		
		cy.getElementById(inDrag).addClass('currentActive');
		cy.getElementById(node.data('id')).addClass('currentActive');
		$('#arrow').css('background-color', '#677aca');
		
		inDrag = null;
	}
};

/** popup */
function hidePagePopup() {
	$('#popPage').hide();
	$('#popLoading').hide();
	$('#popPageContent').hide();
	$('#popAnalyse').hide();
	$('#popImport').hide();
	$('#popAbout').hide();
}

function showPopupAbout() {
	$('#popPage').show();
	$('#popPageContent').show();
	$('#popAbout').show();
}

var setPlotHidden = function() {
	if ($("#enabled")[0].checked) {
		$('#plotted').prop("disabled", false);
		$('#hidden').prop("disabled", false);
	} else {
		$('#hidden').prop("checked", true);
		$('#plotted').prop("checked", false);
		$('#plotted').prop("disabled", true);
		$('#hidden').prop("disabled", true);
	}
}

/** Hides the reaction labels */
function hideReactionLabels() {
	$('#reactionLabel0').hide();
	$('#reactionLabel1').hide();
	$('#reactionLabel2').hide();
}
