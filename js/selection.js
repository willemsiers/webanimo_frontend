var closeSelection = function(e) {
	$('#editNode').hide();
	$('#popBG').hide();
	$('#popContent').hide();
	$('#editEdge').hide();
	$('#leftBarContent').show();
	cy.elements().removeClass('currentActive');
};

var openSelectionNode = function(e) {
	cy.elements().removeClass('currentActive');
	var node = e.cyTarget;
	node.addClass('currentActive');
};

var openSelectionEdge = function(e) {
	cy.elements().removeClass('currentActive');
	var edge = e.cyTarget;
	edge.source().addClass('currentActive');
	edge.target().addClass('currentActive');
	edge.addClass('currentActive');
};