n3Charts.Factory.Tooltip = (function (_super) {
	__extends(Tooltip, _super);
	function Tooltip(element) {
		_super.call(this);
		this.element = element;
	}
	Tooltip.prototype.off = function () {
		_super.prototype.off.call(this);
		this.hide();
	};
	Tooltip.prototype.create = function (options) {
		this.options = options;
		this.createTooltip();
		this.eventMgr.on('container-move.tooltip', this.show.bind(this));
		this.eventMgr.on('container-out.tooltip', this.hide.bind(this));
		this.eventMgr.on('outer-world-hover.tooltip', this.showFromCoordinates.bind(this));
		this.hide();
	};
	Tooltip.prototype.update = function (data, options) {
		this.options = options;
	};
	Tooltip.prototype.createTooltip = function () {
		var svg = this.svg = d3.select(this.element)
			.append('div')
			.attr('class', 'chart-tooltip');
		svg.append('div')
			.attr('class', 'abscissas');
		this.line = this.factoryMgr.get('container').overlay
			.append('line')
			.attr('class', 'tooltip-line');
		this.dots = this.factoryMgr.get('container').overlay
			.append('g')
			.attr('class', 'tooltip-dots');
	};
	Tooltip.prototype.destroy = function () {
		this.svg.remove();
	};


	//var getYForX = function(dataset, x)

	Tooltip.prototype.getRowsToDisplay = function (x, data, options) {
		var visibleSeries = options.series.filter(function(s) { return s.visible});
		var row_serie_pairs = visibleSeries.map(function (serie) {
			var row = data.getDatasetValues(serie, options).filter(serie.defined); 
			return {"row":row, "serie":serie};
		});
		return row_serie_pairs;
	};

	Tooltip.prototype.showFromCoordinates = function (coordinates, data, options) {
		//console.log("showFromCoordinates", coordinates);
		if (this.isOff()) {
			return;
		}
		var x = coordinates.x, y = coordinates.y;
		if (x === undefined || y === undefined) {
			this.hide('x or y undefined', data, options);
			return;
		}
		if (x instanceof Date) {
			// _sigh_ TypeScript...
			x = x.getTime();
		}
		var row_serie_pairs= this.getRowsToDisplay(x, data, options);

		if (row_serie_pairs.length === 0) {
			this.hide('length=0', data, options);
			return;
		}
		this.updateTooltipDots(row_serie_pairs);
		this.dots.style('opacity', '1');
		this.line.style('opacity', '1');
		var tooltipContent = this.getTooltipContent(row_serie_pairs, x);
		if (!tooltipContent) {
			return;
		}
		this.updateTooltipContent(tooltipContent);
		this.updateTooltipAndLinePosition(x);
		this.svg.style('display', null);
	};
	Tooltip.prototype.show = function (event, data, options) {
		if (this.isOff()) {
			return;
		}
		var container = this.factoryMgr.get('container');
		var coordinates = container.getCoordinatesFromEvent(event);
		this.showFromCoordinates(coordinates, data, options);
	};
	Tooltip.prototype.hide = function (event, data, options) {
		//prevent mouseout events that are still in the chart (occurs in Firefox, not Chrome)
		if( event &&  event.type === 'mouseout')
		{
			var $related = $(event.relatedTarget);
			var parents = $related.parentsUntil( $('#resultChart') );
			var upperParent = parents[parents.length-1].parentElement;
			if(upperParent && upperParent.id === "resultChart")
			{
				return;
			}
		}
		this.svg
			.style('display', 'none');
		this.line
			.style('opacity', '0');
		this.dots
			.style('opacity', '0');
		if (options && options.tooltipHook) {
			options.tooltipHook(undefined);
		}

	};
	// This is the part the user can override.
	Tooltip.prototype.getTooltipContent = function (row_serie_pairs, x) {
		var interpolationFunction = n3Charts.Factory.Tooltip.animoInterpolationFunction;
		var getRowValue = function (row) {
			return interpolationFunction(row, x);
		};

		var seconds = Math.floor((x % 1) * 60) + '';
		if (seconds.length < 2) seconds = '0' + seconds;

		return {
			abscissas: 'Time: ' + Math.floor(x) + ':' + seconds,
			rows: row_serie_pairs.map(function (pair) {
				return {
					label: pair.serie.label,
					value: getRowValue(pair.row),
					color: pair.serie.color,
					id: pair.serie.id
				};
			})
		};
	};
	Tooltip.prototype.updateTooltipContent = function (result) {
		this.svg.select('.abscissas')
			.text(result.abscissas);
		var initItem = function (s) {
			s.attr({ 'class': 'tooltip-item' });
			s.append('div')
				.attr({ 'class': 'color-dot' })
				.style({
					'background-color': function (d) { return d.color; }
				});
			s.append('div')
				.attr({ 'class': 'series-label' });
			s.append('div')
				.attr({ 'class': 'y-value' });
			return s;
		};
		var updateItem = function (s) {
			s.select('.series-label')
				.text(function (d) { return d.label; });
			s.select('.y-value')
				.text(function (d) { return Math.floor(d.value); });
			return s;
		};
		var items = this.svg.selectAll('.tooltip-item')
			.data(result.rows, function (d, i) { return !!d.id ? d.id : i; });
		items.enter()
			.append('div')
			.call(initItem)
			.call(updateItem);
		items.call(updateItem);
		items.exit().remove();
	};
	Tooltip.prototype.updateTooltipDots = function (rows) {
		/**
		var _this = this;
		var xScale = this.factoryMgr.get('x-axis').scale;
		var yScale = function (side) { return _this.factoryMgr.get(side + '-axis').scale; };
		var radius = 3;
		var circlePath = function (r, cx, cy) {
			return "M " + cx + " " + cy + " m -" + r + ", 0 a " + r + "," + r + " 0 1,0 " + r * 2 + ",0 a " + r + "," + r + " 0 1,0 -" + r * 2 + ",0 ";
		};
		var initDots = function (s) {
			s.attr('class', 'tooltip-dots-group');
			s.append('path').attr({
				'class': 'tooltip-dot y1'
			}).on('click', function (d, i) {
				_this.eventMgr.trigger('click', d.row, i, d.series, _this.options);
			});
			s.append('path').attr({
				'class': 'tooltip-dot y0'
			}).style({
				'display': function (d) { return d.series.hasTwoKeys() ? null : 'none'; }
			}).on('click', function (d, i) {
				_this.eventMgr.trigger('click', d.row, i, d.series, _this.options);
			});
		};
		var updateDots = function (s) {
			s.select('.tooltip-dot.y1').attr({
				'd': function (d) { return circlePath(radius, xScale(d.row.x), yScale(d.series.axis)(d.row.y1)); },
				'stroke': function (d) { return d.series.color; }
			});
			s.select('.tooltip-dot.y0').attr({
				'd': function (d) {
					if (d.series.hasTwoKeys()) {
						return circlePath(radius, xScale(d.row.x), yScale(d.series.axis)(d.row.y0));
					}
					return '';
				},
				'stroke': function (d) { return d.series.color; }
			});
		};
		var dots = this.dots.selectAll('.tooltip-dots-group')
			.data(rows);
		dots.enter()
			.append('g')
			.call(initDots)
			.call(updateDots);
		dots.call(updateDots);
		dots.exit().remove();
		 **/
	};

	Tooltip.prototype.updateTooltipAndLinePosition = function (x) {
		var xAxis = this.factoryMgr.get('x-axis');
		var yScale = this.factoryMgr.get('y-axis').scale;
		var container = this.factoryMgr.get('container');
		var dim = container.getDimensions();
		var margin = dim.margin;
		var leftOffset = this.element.offsetLeft;
		var topOffset = this.element.offsetTop;
		var xOffset = 0;
		var transform = '';
		if (xAxis.isInLastHalf(x)) {
			transform = 'translate(-100%, 0)';
			xOffset = -10;
		}
		else {
			xOffset = 10;
		}
		this.svg
			.style({
				'left': (leftOffset + margin.left + xAxis.scale(x) + xOffset) + 'px',
				'top': (topOffset + margin.top) + 'px',
				'transform': transform
			});

		var lineX = (xAxis.scale(x) );

		this.line.attr({
			'x1': lineX,
			'x2': lineX,
			'y1': -dim.margin.top,
			'y2': dim.innerHeight
		});
		return;
	};
	return Tooltip;
})(n3Charts.Factory.BaseFactory);
