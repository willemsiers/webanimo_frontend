<?php

if (empty($_POST['network']) || json_decode($_POST['network']) == null){
	die('{ "error" : "Invalid or no network provided" }');
}

$jarfile = './animo-latest.jar';
$pullscript = 'bash ./update_jar.sh';

if (!file_exists($jarfile))
	exec($pullscript);

//$_POST['network']; //str_replace('"', "'", $_POST['network']);

// write temp
$tmp_file = tempnam(sys_get_temp_dir(), 'animo_');
file_put_contents($tmp_file, $_POST['network']);

// execute value
$result = exec('java -jar '.$jarfile.' '.escapeshellarg($tmp_file));//, $output);

// remove temp
@unlink($tmp_file);


$result_object = json_decode($result, true);
//echo $result_object['error'];
if(!empty($result_object['error']))
{
	http_response_code(400); //bad request
}else{
	http_response_code(200); //OKkk
}

echo $result;

?>
