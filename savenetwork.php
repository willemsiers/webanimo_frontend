<?php
if (empty($_POST['network']) || json_decode($_POST['network']) == null){
	die('{ "error" : "Invalid or no network provided" }');
}

$location = './networks';
$locksloc = './locks';

$file = null;
$lockfile = null;
$ip = $_SERVER['REMOTE_ADDR'];
if (empty($_GET['network'])) {

	// get file to write to
	$file = getRandFile($location);
	$lockfile = $locksloc . '/' . basename($file);

	// acquire lock
	file_put_contents($lockfile, $ip); 
	chmod($lockfile, 0600);

} else {

	$file = preg_replace('/[^a-zA-Z0-9]/', '', $_GET['network']);
	if (!file_exists($location . '/' . $file)) {
		die('{"error" : "This network does not exist!" }');
	}
	if (!file_exists($locksloc . '/' . $file)) {
		die('{"error" : "No lock acquired"}');
	}
	if (file_get_contents($locksloc . '/' . $file) != $ip) {
		echo "file".file_get_contents($locksloc . '/' . $file);
		echo "ip" . $ip;
		die('{"error" : "Lock acquired by different user" }');
	}

	$file = $location . '/' . $file;

}

file_put_contents($file, $_POST['network']);

echo '{"file": "' . basename($file) . '"}';
return;


function getRandFile($location) {

	$file = '';

	do {
		$mdhash = md5(mt_rand());
		$file = $location . '/' . $mdhash;
	} while(file_exists($file));

	// touch file to prevent concurrency errors and set permissions to default
	touch($file);
	chmod($file, 0600);

	// assert the impossible
	if ($file == '') die;

	return $file;

}
